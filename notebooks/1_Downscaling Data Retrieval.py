# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,md
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: 'Python 3.8.1 64-bit (''3.8.1'': pyenv)'
#     language: python
#     name: python38164bit381pyenvb3df2019228041fdaa1b32128074601a
# ---

# # 1 Downscaling Data Retrieval

# The purpose of this notebook is to compile the data that will be used to downscale the following notebook. Most specifically, we need to have:
#
# * GDP (Current and Projections)
# * Population (Current and Projections)
# * Energy Mix (Current)
# * Emissions (Current)
#
# Since we are focusing on the Netherlands, we will use regional data from the respective countries and Eurostat. Where possible, we will rely on APIs. The `eurostat` package on PyPi is an unofficial API that ties into the official Eurostat databases. Greenhouse gas emissions data is from the EU Emissions Database for Global Atmospheric Research.

import pandas as pd
import eurostat

eurostatTOC = eurostat.get_toc_df()
eurostatTOC.head()

# A list of flag codes for Eurostat data is avaialble at https://ec.europa.eu/eurostat/statistics-explained/index.php/Tutorial:Symbols_and_abbreviations
#
# The data Eurostat tracks includes EU candidate countries, like Albania. We need to filter those out. It also uses codes raather than the full English name of thhose countries. A dictionary will be useful to work back and forth with these.
#
# **EU Countries and their Codes**
#
# |Country|Code|Country|Code|Country|Code|Country|Code|
# |--|--|--|--|--|--|--|--|
# Belgium|(BE)|Greece|(EL)|Lithuania|(LT)|Portugal|(PT)
# Bulgaria|(BG)|Spain|(ES)|Luxembourg|(LU)|Romania|(RO)
# Czechia|(CZ)|France|(FR)|Hungary|(HU)|Slovenia|(SI)
# Denmark|(DK)|Croatia|(HR)|Malta|(MT)|Slovakia|(SK)
# Germany|(DE)|Italy|(IT)|Netherlands|(NL)|Finland|(FI)
# Estonia|(EE)|Cyprus|(CY)|Austria|(AT)|Sweden|(SE)
# Ireland|(IE)|Latvia|(LV)|Poland|(PL)|*United Kingdom*|*(UK)*|
#
# Furthermore, since GCAM only has EU-12, EU-15, Europe non-EU, and Eastern Europe as regions, we must further limit our data. Since EU-12 sometimes only includes West Germany, we will use EU-15 (which also includes the United Kingdom, though as of writing, Brexit as occured).

# These are EU countries only
# countryMap = {'BE':'Belgium','EL':'Greece','LT':'Lithuania','PT':'Portugal','BG':'Bulgaria','ES':'Spain','LU':'Luxembourg','RO':'Romania','CZ':'Czechia','FR':'France','HU':'Hungary','SI':'Slovenia','DK':'Denmark','HR':'Croatia','MT':'Malta','SK':'Slovakia','DE':'Germany','IT':'Italy','NL':'Netherlands','FI':'Finland','EE':'Estonia','CY':'Cyprus','AT':'Austria','SE':'Sweden','IE':'Ireland','LV':'Latvia','PL':'Poland'}
# These are EU-15 countries
countryMap = {'BE':'Belgium','EL':'Greece','PT':'Portugal','ES':'Spain','LU':'Luxembourg','FR':'France','DK':'Denmark','DE':'Germany','IT':'Italy','NL':'Netherlands','FI':'Finland','AT':'Austria','SE':'Sweden','IE':'Ireland','UK':'United Kingdom'}
len(countryMap)

countryCodes = [i for i in countryMap.keys()]
countryNames = [i for i in countryMap.values()]

# ## Population

eurostat.subset_toc_df(eurostatTOC, 'population')

dfPopulation = eurostat.get_data_df('demo_r_d2jan', flags=False)
dfPopulation.head()

# Rename the weird column header `geo\time` to `geo`
dfPopulation.rename(columns={'geo\\time':'geo'}, inplace=True)

dfPopulation = dfPopulation.query("geo.isin(@countryCodes) & age == 'TOTAL' & sex=='T'")
# Note: the `@` before `countryCodes` connects the .query() environment to the global Python environment

# Let's turn this wide table into a long one, which will better suit our needs.

dfPopulation = dfPopulation.drop(columns=['sex','age']).melt(id_vars=['geo','unit'], var_name='Year')

# Keep only the 2019 population data.

dfPopulation.to_csv('../data/interim/eurostatPopulation.csv')

# ## GDP

eurostat.subset_toc_df(eurostatTOC, 'GDP')

dfGDP = eurostat.get_data_df('nama_10_gdp', flags=False)
dfGDP.head()

# 'na_item' B1GQ refers to 'Gross domestic product at market prices'.
# 'unit' refers to various ways of expressing the value of money. 'CLV15_MEUR' refers to 'chain linked volumes' (it accounts for inflation over time) in 2015 prices. 'CP_MEUR' is the current price in millions of Euros (this form of analysis will change over time due to the fluctuating value of the Euro).

# +
# Rename the weird column header `geo\time` to `geo`
dfGDP.rename(columns={'geo\\time':'geo'}, inplace=True)

dfGDP = dfGDP.query("geo.isin(@countryCodes) & na_item == 'B1GQ' & unit == 'CLV15_MEUR'")
# Note: the `@` before `countryCodes` connects the .query() environment to the global Python environment
# -

# Unfortunately, GCAM uses the US dollar at 1990 values. Some foreign exchange databases do not go back to 1991, so to make these values commensurable, we must convert the currencies and apply an inflation consideration.
#
# According to the American Bureau of Labour Statistics, '$1 in January 1990 has the same buying power as $1.83 in January 2015'.
#
# * https://www.bls.gov/data/inflation_calculator.htm
#
# The conversion rate between the Euro and the American Dollar for January 2015 was '€1.1096/$1'.
#
# * https://fred.stlouisfed.org/series/DEXUSEU

EUR2015toUSD1990 = 1/1.1096/1.83

dfGDP.iloc[:,3:] = dfGDP.iloc[:,3:] * EUR2015toUSD1990
dfGDP['unit'] = 'MUSD_1990'

dfGDP = dfGDP.drop(columns='na_item').melt(id_vars=['geo','unit'], var_name='Year')

dfGDP

# +
# Sanity check
dfGDP.query("Year==2019")['value'].sum()

# Note that this value differs from the published EU-15 value of €14,700,087.3 million for some reason.
# -

dfGDP.to_csv('../data/interim/eurostatGDP.csv')

# ## Emissions

# From the EU Joint Research Centre "[Fossil CO2 and GHG emissions of all world countries, 2019 report](https://edgar.jrc.ec.europa.eu/overview.php?v=booklet2019)".
#
# Here, we will use the CO2 emission data instead of the total GHG data because the former are updated to 2018 and the latter to 2015.
#
# - Crippa, M., Oreggioni, G., Guizzardi, D., Muntean, M., Schaaf, E., Lo Vullo, E., Solazzo, E., Monforti-Ferrario, F., Olivier, J.G.J., Vignati, E., Fossil CO2 and GHG emissions of all world countries - 2019 Report, EUR 29849 EN, Publications Office of the European Union, Luxembourg, 2019, ISBN 978-92-76-11100-9, doi:10.2760/687800, JRC117610.

dfEmissions = pd.read_excel(
    "https://edgar.jrc.ec.europa.eu/booklet2019/EDGARv5.0_FT2018_fossil_CO2_GHG_booklet2019.xls",
    sheet_name="fossil_CO2_by_sector_and_countr")
dfEmissions.head()

# EDGAR lists country names slightly differently than the Eurostat database. Technically, this means that the emissions included here go above the EU-15, but the secondary regions included are also very small. We will treat them as the larger region only.

countryNamesEDGAR = {'France and Monaco':'France',
                     'Spain and Andorra':'Spain',
                     'Italy, San Marino and the Holy See':'Italy'}
dfEmissions.replace(countryNamesEDGAR, inplace = True)

# Change all names back to codes
countryMap_namesToCode = {v: k for k, v in countryMap.items()}
dfEmissions.replace(countryMap_namesToCode, inplace = True)

# Change the location column header to the same as our other data
dfEmissions.rename(columns={'country_name':'geo'}, inplace=True)

# Since we are only looking at the electricity sector, we will also only look at the emissions from it.

dfEmissions = dfEmissions.query("geo.isin(@countryCodes) & Sector == 'Power Industry'")
# Note: the `@` before `countryCodes` connects the .query() environment to the global Python environment

dfEmissions

# Let's turn this wide table into a long one, which will better suit our needs.

dfEmissions = dfEmissions.drop(columns='Sector').melt(id_vars=['geo'], var_name='Year')
dfEmissions['unit'] = 'Mt CO2eq/yr'

dfEmissions.info()

dfEmissions

dfEmissions.to_csv('../data/interim/EDGAREmissions.csv')

# ## Energy
#
# Electricity Production by Fuel

# +
# Backup data source: https://www.eea.europa.eu/data-and-maps/daviz/gross-electricity-production-by-fuel-4/download.csv
# EU Environment Agency https://www.eea.europa.eu/data-and-maps/daviz/gross-electricity-production-by-fuel-4#tab-chart_2
# -

dfEnergy = eurostat.get_data_df('nrg_cb_pem', flags=False)
dfEnergy.head()

# Rename the weird column header `geo\time` to `geo`
dfEnergy.rename(columns={'geo\\time':'geo'}, inplace=True)

dfEnergy = dfEnergy.query("geo.isin(@countryCodes)")

# ### Dataset Formatting
#
# Unlike the other datasets, let's turn this wide format into a long one first to aggregate the monthly data into years.

# #### Wide to Long

dfEnergy = dfEnergy.set_index(['geo', 'unit','siec']).rename_axis(['Year'], axis=1).stack().reset_index()\
    .rename(columns={'Year':'year',0:'value'})

dfEnergy.head()

# Make sure the value still contains numeric values.

dfEnergy['value'].sum()

dfEnergy.info()

# #### Aggregating Months into Years
# Let's sum up all the years together.

dfEnergy['year'].unique()

dfEnergy['year'] = dfEnergy['year'].str[:-3]

dfEnergy.head()

dfEnergy = dfEnergy.groupby(['geo','unit','siec','year']).sum().reset_index()

dfEnergy

dfEnergy['value'].sum()

# The sum of the values column is still the same as earlier.

# ### Dataset Semantic Validation
#
# This dataset comes with a lot of different types of information. It is important we quickly validate what the set contains and doesn't.

dfEnergy['unit'].unique()

# #### Resource Type and Usage Semantic Check

# Standard Interntional Energy Code (SIEC) is a code for the type of fuel used. The SIEC categories are "exhaustive and mutually exclusive" (United Nations, 2018, pp. 23). However, it pertains only to fuel directly, so renewables are not counted – electricity is instead recordedd. For internal data purposes, Eurostat generally follows SIEC guidance and maintains a separate list of nomenclature for interim data, like renewables, which is listed below.
#
# * United Nations. (2018). Standard International Energy Product Classification. In International Recommendations for Energy Statistics (pp. 21–40). United Nations Publications. https://doi.org/10.18356/2684a9ee-en
# * https://ec.europa.eu/eurostat/documents/38154/4956218/ENERGY-BALANCE-GUIDE-DRAFT-31JANUARY2019.pdf/cf121393-919f-4b84-9059-cdf0f69ec045
# * https://ec.europa.eu/eurostat/ramon/nomenclatures/index.cfm

dfEnergy['siec'].unique()

# The SIEC fuel codes includes many categories and subcategories.
#
# |Code|Resource Type|
# |--|--|
# |CF|Combustible fuels|
# |CF_R|Combustible fuels - renewable|
# |CF_NR|Combustible fuels - non-renewable|
# |C0000|Coal and manufactured gases|
# |G3000|Natural gas|
# |O4000XBIO|Oil and petroleum products (excluding biofuel portion)|
# |RA100|Hydro|
# |RA110|Pure hydro power|
# |RA120|Mixed hydro power|
# |RA130|Pumped hydro power|
# |RA200|Geothermal|
# |RA300|Wind|
# |RA310|Wind on shore|
# |RA320|Wind off shore|
# |RA400|Solar|
# |RA410|Solar thermal|
# |RA420|Solar photovoltaic|
# |RA500_5160|Other renewable energies|
# |N9000|Nuclear fuels and other fuels n.e.c.|
# |X9900|Other fuels n.e.c.|

siecMap = {
'CF':'Combustible fuels',
'CF_R':'Combustible fuels - renewable',
'CF_NR':'Combustible fuels - non-renewable',
'C0000':'Coal and manufactured gases',
'G3000':'Natural gas',
'O4000XBIO':'Oil and petroleum products (excluding biofuel portion)',
'RA100':'Hydro',
'RA110':'Pure hydro power',
'RA120':'Mixed hydro power',
'RA130':'Pumped hydro power',
'RA200':'Geothermal',
'RA300':'Wind',
'RA310':'Wind on shore',
'RA320':'Wind off shore',
'RA400':'Solar',
'RA410':'Solar thermal',
'RA420':'Solar photovoltaic',
'RA500_5160':'Other renewable energies',
'N9000':'Nuclear fuels and other fuels n.e.c.',
'X9900':'Other fuels n.e.c.'
}

dfEnergy['siecName'] = dfEnergy['siec'].replace(siecMap)

dfEnergy.query("siecName!='TOTAL'").groupby('siecName').sum().unstack().plot.bar()

dfEnergy.query("geo=='NL' & year=='2018'")

# At a glance, we can see that `CF` (combustible fuels) is not simply the sum of `CF_NR` (non-renewable) and `CF_R` (renewable), which is unexpected. Yet, `RA100` (hydro) includes the 'pure', 'mixed', and 'pumped' types. SIEC differentiates such categories as 'levels', but what `CF` is isn't very well documented with the EU's documents.

dfEnergy.query("geo=='NL' & year=='2018' & siec.isin(['C0000','CF_R','CF_NR','G3000','O4000XBIO'])")['value'].sum()

# It seems like 'Coal and manufactured gases', 'Combustible fuels - non-renewable', 'Combustible fuels - renewable', 'Natural gas', and 'Oil and petroleum products (excluding biofuel portion)' is 25 GWH short of 'Combustible fuels'. Perhaps the biofuel section of oil and petroleum products constitutes the remaining 25 GWH.
#
# Let's check this for another year and country.

dfEnergy.query("geo=='NL' & year=='2017' & siec.isin(['CF'])")['value'].sum()

dfEnergy.query("geo=='NL' & year=='2017' & siec.isin(['C0000','CF_R','CF_NR','G3000','O4000XBIO'])")['value'].sum()

dfEnergy.query("geo=='DE' & year=='2017' & siec.isin(['CF'])")['value'].sum()

dfEnergy.query("geo=='DE' & year=='2017' & siec.isin(['C0000','CF_R','CF_NR','G3000','O4000XBIO'])")['value'].sum()

dfEnergy.query("geo=='DK' & year=='2017' & siec.isin(['CF'])")['value'].sum()

dfEnergy.query("geo=='DK' & year=='2017' & siec.isin(['C0000','CF_R','CF_NR','G3000','O4000XBIO'])")['value'].sum()

dfEnergy.query("geo=='UK' & year=='2017' & siec.isin(['CF'])")['value'].sum()

dfEnergy.query("geo=='UK' & year=='2017' & siec.isin(['C0000','CF_R','CF_NR','G3000','O4000XBIO'])")['value'].sum()

# It seems like for a small sample of other countries, these categories match. Yet, in the Netherlands, they do not. There must be a data issue or our understanding here is incomplete.

# #### Unit Matching

# GCAM uses exajoules for energy, not GWh. Let's convert as such.
#
# $$\frac{3.6\times10^{-6} EJ}{1 GWh}$$
#
# _**These units should not be italicized**_

dfEnergy['value'] = dfEnergy['value'] * (3.6 * 10 ** -6)
dfEnergy['unit'] = 'EJ'

# ### Dataset Final Shaping and Export

# #### For the Convergence Approach

# Half of the intent for retrieving this dataset is to use it as baseline data to use and compare with GCAM data. For that reason, it is important to match up the sectors to the ones we actually want to use and keep. This linear approach will also be applied to this recategorized data for comparison.
#
# The GCAM categories are:
#
# >'coal (conv pul)', 'coal (IGCC)', 'gas (steam/CT)', 'gas (CC)', 'biomass (conv)', 'biomass (IGCC)', 'Gen_II_LWR', 'Gen_III', 'hydro', 'wind_storage', 'wind', 'PV', 'PV_storage', 'CSP', 'CSP_storage', 'geothermal', 'refined liquids (steam/CT)', 'refined liquids (CC)', 'rooftop_pv', 'biomass cogen', 'coal cogen', 'gas cogen', 'hydrogen cogen', and 'refined liquids cogen'.
#
# They actually distinguish fuel use in different types of facilities, which makes sense because the technologies that underlie each of these will change between now and the future. Some of these categories do not yet exist, so we can ignore them. Below is a table matching the Eurostat data to GCAM.

# |GCAM Category|Eurostat Categories|
# |--|--|
# |biomass (conv), biomass (IGCC), biomass cogen|Combustible fuels - renewable, Combustible fuels - non-renewable|
# |coal (conv pul), coal (IGCC), coal cogen|Coal and manufactured gases|
# |gas (steam/CT), gas (CC), gas cogen|Natural gas|
# |refined liquids (steam/CT), refined liquids (CC), refined liquids cogen|Oil and petroleum products (excluding biofuel portion)|
# |hydro|Hydro|
# |geothermal|Geothermal|
# |wind, wind_storage|Wind|
# |CSP, CSP_storage|Solar thermal|
# |PV, PV_storage, rooftop_pv|Solar photovoltaic|
# |hydrogen cogen|Other renewable energies|
# |Gen_II_LWR, Gen_III|Nuclear fuels and other fuels n.e.c.|
# |_None_|Other fuels n.e.c.|
#
# Note: biomass renewable is problematic lmao - check document
#
# ** What input data does GCAM use?**

siec2GCAMMapping = {'Biomass': 'Combustible fuels - renewable',
                   'Biomass': 'Combustible fuels - non-renewable',
                   'Coal':'Coal and manufactured gases',
                   'Natural gas': 'Natural gas', # redundant but included for completeness
                   'Oil': 'Oil and petroleum products (excluding biofuel portion)',
                   'Hydro': 'Hydro', # redundant but included for completeness
                    'Geothermal': 'Geothermal', # redundant but included for completeness
                    'Wind': 'Wind', # redundant but included for completeness
                    'Solar Thermal': 'Solar thermal',
                    'Solar PV': 'Solar photovoltaic',
                    'Hydrogen': 'Other renewable energies', # Potentially untrue
                    'Nuclear': 'Nuclear fuels and other fuels n.e.c.'
                   }
siec2GCAMMapping = {v: k for k, v in siec2GCAMMapping.items()}

dfEnergy['siecName'] = dfEnergy['siecName'].replace(siec2GCAMMapping)

dfEnergy['siecName'].unique()

dfEnergy = dfEnergy.query("siecName.isin(['Biomass', 'Coal', 'Natural gas', 'Oil', 'Hydro', 'Geothermal', 'Wind', 'Solar Thermal', 'Solar PV', 'Hydrogen', 'Nuclear'])")

dfEnergy = dfEnergy.drop(columns='siec')

dfEnergy.query("geo=='NL'")['value'].sum()

dfEnergy.to_csv('../data/interim/eurostatEnergy.csv')


