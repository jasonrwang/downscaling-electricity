---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.3.2
  kernelspec:
    display_name: 'Python 3.8.1 64-bit (''3.8.1'': pyenv)'
    language: python
    name: python38164bit381pyenvb3df2019228041fdaa1b32128074601a
---

# 2 GCAM Results Downscaling


This notebook takes the results of interest from GCAM and downscales them.

This is inspired by the IIASA downscaling toolkit, available on GitHub at [iiasa/emissions_downscaling](https://github.com/iiasa/emissions_downscaling/) and discussed by Gidden et al. (2019), and Sferra et al.'s (2019) discussion of model-based dynamic emissions downscaling.

* Gidden, M. J., Riahi, K., Smith, S. J., Fujimori, S., Luderer, G., Kriegler, E., … Takahashi, K. (2019). Global emissions pathways under different socioeconomic scenarios for use in CMIP6: a dataset of harmonized emissions trajectories through the end of the century. Geoscientific Model Development, 12(4), 1443–1475. https://doi.org/10.5194/gmd-12-1443-2019
* Sferra, F., Krapp, M., Roming, N., Schaeffer, M., Malik, A., Hare, B., & Brecha, R. (2019). Towards optimal 1.5° and 2 °C emission pathways for individual countries: A Finland case study. Energy Policy, 133(April 2019), 110705. https://doi.org/10.1016/j.enpol.2019.04.020

```python
import pandas as pd
from os import listdir
import altair as alt # for interim verification and communication
```

Define a theme that deals with more than 10 categorical values (i.e. we have 15 countries and the default behaviour only works with the 10 here https://vega.github.io/vega/docs/schemes/#categorical).

```html
<style>
@import url('https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap');
</style>
```

```python
def my_theme():
    return {
        'config': {'range': {'category': {'scheme': 'category20'}},
                    'font': 'Source Sans Pro'}
    }

alt.themes.register('my_theme', my_theme)
alt.themes.enable('my_theme')
```

<!-- #region toc-hr-collapsed=true toc-nb-collapsed=true -->
## Load Interim GCAM Data from Previous Notebook

This version of reference outputs is from GCAM 5.2 using the default reference configuration file.
<!-- #endregion -->

```python
files = listdir("../data/interim")
files
```

### Energy

```python
# interestingFile = files[3]
interestingFile = 'GCAM_Electricity generation by technology (inc solar roofs).csv'
dfGCAM_energy = pd.read_csv('../data/interim/' + interestingFile, index_col=0, parse_dates=['Year'])
```

```python
dfGCAM_energy.head()
```

For some reason, the cogen sectors here contain data from 1975, which do not show in GCAM's interface.

```python
dfGCAM_energy = dfGCAM_energy.query("region == 'EU-15' & Year >= 1990")
```

#### Match GCAM Energy Sectors with Eurostat Data

Unfortunately, Eurostat data does not differentiate energy use by technology type like GCAM does, so we need to do some mapping to aggregate some data for the convergence methodology. First, we will save the data as it is as another variable for the linear downscaling portion to retain the technology types. We will also eventually show the linear downscaling with the aggregated sectors too.

```python
dfGCAM_energyLinear = dfGCAM_energy.copy()
dfGCAM_energyRecategorized = dfGCAM_energy.copy()
```

```python
dfGCAM_energyLinear.to_csv('../data/processed/GCAM_energyOriginal.csv')
```

```python
GCAMtoFuelMapping = {
    'Biomass': ['biomass (conv)', 'biomass (IGCC)', 'biomass cogen'],
    'Coal':['coal (conv pul)', 'coal (IGCC)', 'coal cogen'],
    'Natural gas': ['gas (steam/CT)', 'gas (CC)', 'gas cogen'],
    'Oil': ['refined liquids (steam/CT)', 'refined liquids (CC)', 'refined liquids cogen'],
    'Hydro': 'hydro',
    'Geothermal': 'geothermal',
    'Wind': ['wind', 'wind_storage'],
    'Solar Thermal': ['CSP', 'CSP_storage'],
    'Solar PV': ['PV', 'PV_storage', 'rooftop_pv'],
    'Hydrogen': 'hydrogen cogen',
    'Nuclear': ['Gen_II_LWR', 'Gen_III']
}
```

```python
GCAMtoFuelMappingInverse = {}
for fuel, fuelTechs in GCAMtoFuelMapping.items():
    if type(fuelTechs) == list:
        for technology in fuelTechs:
            GCAMtoFuelMappingInverse.update({technology: fuel})
    else:
        GCAMtoFuelMappingInverse.update({fuelTechs: fuel})
```

```python
GCAMtoFuelMappingInverse
```

```python
dfGCAM_energyRecategorized['technology'] = dfGCAM_energyRecategorized['technology'].replace(GCAMtoFuelMappingInverse)
```

```python
dfGCAM_energyRecategorized
```

```python
dfGCAM_energyRecategorized = dfGCAM_energyRecategorized.groupby(['Units','scenario','region','technology','Year']).sum().reset_index()
```

```python
dfGCAM_energyRecategorized.head()
```

```python
dfGCAM_energyRecategorized.to_csv('../data/processed/GCAM_energyRecategorized.csv')
```

### GDP

```python
dfGCAM_GDP = pd.read_csv('../data/interim/GCAM_GDP MER by region.csv', index_col=0, parse_dates=['Year'])
```

```python
dfGCAM_GDP.head()
```

```python
dfGCAM_GDP = dfGCAM_GDP.query("region == 'EU-15'")
```

### Population

```python
dfGCAM_population = pd.read_csv('../data/interim/GCAM_population by region.csv', index_col=0, parse_dates=['Year'])
```

```python
dfGCAM_population.head()
```

```python
dfGCAM_population = dfGCAM_population.query("region == 'EU-15'")
```

### Emissions

```python
dfGCAM_emissions = pd.read_csv('../data/interim/GCAM_CO2 emissions by region and sector.csv', index_col=0, parse_dates=['Year'])
```

```python
dfGCAM_emissions.head()
```

```python
dfGCAM_emissions
```

```python
dfGCAM_emissions.rename(columns={'sector.1':'Year'}, inplace=True)
dfGCAM_emissions['sector'].unique()
```

_I should use elec sector only emissions... somehow, GCAM and EDGAR values are very off._

```python
dfGCAM_emissions = dfGCAM_emissions.query("region == 'EU-15' & sector.str.startswith('elec')")
```

```python
dfGCAM_emissions
```

```python
dfGCAM_emissions = dfGCAM_emissions.groupby(['scenario','Year'], as_index=False).sum() # as_index=False disallows multi-indexing
dfGCAM_emissions['Units'] = 'MTC'
```

```python
dfGCAM_emissions.head()
```

## Downscaling Methodology

Van Vuuren et al. (2007) describe several general approaches to downscaling. In their 2019 paper, Sferra et al. implemented a smaller "meta-model" that takes IAM results as boundary conditions.

* Conditional modelling
* Algorithms
    * Linear
    * Convergence (partial and full)
    * External-input-based
* Dynamical model **(is this the same as conditional modelling tho)**

Gidden et al. (2019) use the convergence method with the IPAT equation for the energy sector, which assumes that the energy intensity of all the constituents of a model region will converge in some future date. They used different convergence years based on the underlying SSP scenarios.

However, the convergence method assumes an eventually homogenous distribution of technologies. The linear apaproach is even more simple in that it assumes no distribution change between now and the future of whatever underlying factor it uses. Thought not used often, it might seem reasonable in some applications to downscale proportionally to GDP or population, using these factors as proxies for technological mixes or preferences.

This analysis will use linear downscaling for:

* Population (using the GCAM model's underlying population change scenario or UN ones (van Vuuren et al. argue that these are good and "authohritative"))
* GDP (again, using the GCAM model) – both relative to current populations (or someone's projection within a region???)
* Mix of industry to commercial and residential energy demand (industry less flexible?)

It will use the convergence method for:

* Emissions intensity, as implemented in van Vuuren (2007) and Gidden et al. (2019)
* Energy use (can also be extended up to emissions using emission factors for comparison)


### Country of Interest

Pick the country to downscale to!

```python
countryofInterest = 'NL'
```

#### Define the Base Year

```python
baseYear = 2018
```

### Linear Downscaling

* Pros: logically intuitive and algoithmically simple
* Cons: internally inconsistent


#### Population

Using static 2019 levels and using UNDP forecasts


##### Static

```python
dfPopulation = pd.read_csv('../data/interim/eurostatPopulation.csv', index_col=0, parse_dates=['Year'])
```

Normalize the population of each country to the sum of the whole group.

```python
dfPopulationProportion = dfPopulation.copy()
dfPopulationProportion['value'] = dfPopulation.set_index(['Year','geo'])['value'].values/ dfPopulation.groupby(['Year'])['value'].transform('sum')
dfPopulationProportion['unit'] = 'PR'
```

```python
dfPopulationProportion.query("Year==@baseYear")
```

```python
dfPopulationProportion.query("Year==@baseYear")['value'].sum()
```

```python
dfGCAM_energyRecategorized
```

```python
dfData_linearPopulation = pd.DataFrame()
dfData_linearTemp = pd.DataFrame()
for country in dfPopulation['geo'].unique():
    dfData_linearTemp = dfGCAM_energyRecategorized.copy()
    dfData_linearTemp['region'] = country
    dfData_linearTemp['value'] = \
        dfGCAM_energyRecategorized['value'] * dfPopulationProportion.query("geo==@country & Year==@baseYear")['value'].values
    dfData_linearPopulation = dfData_linearPopulation.append(dfData_linearTemp)

dfData_linearPopulation = dfData_linearPopulation.reset_index(drop=True)
```

```python
dfData_linearPopulation
```

```python
dfData_linearPopulation.to_csv('../data/processed/linearPopulation_Recategorized.csv')
```

This above approach is compiled as a single function, `linearDownscale()`, within the folder `src`.

```python
from myFuncs import linearDownscale
```

```python
linearDownscale(dfGCAM_energyRecategorized,dfPopulation,'Population_Recategorized',baseYear)
```

```python
linearDownscale(dfGCAM_energyLinear,dfPopulation,'Population_Original',baseYear)
```

##### Forecasts

Linearly proportional to the forecast population per year (interpolate where not possible).

```python

```

<!-- #region toc-hr-collapsed=true toc-nb-collapsed=true -->
#### Using GDP

How can regional and country-level GDP forecasts be reconciled under the different scenarios?
<!-- #endregion -->

##### Static

```python
dfGDP = pd.read_csv('../data/interim/eurostatGDP.csv', index_col=0, parse_dates=['Year'])
```

```python
linearDownscale(dfGCAM_energyRecategorized,dfGDP,'GDP_Recategorized',baseYear)
linearDownscale(dfGCAM_energyLinear,dfGDP,'GDP_Original',baseYear)
```

##### Forecasts

```python

```

#### Using Current Emissions


##### Static

```python
dfEmissions = pd.read_csv('../data/interim/EDGAREmissions.csv', index_col=0, parse_dates=['Year'])
```

```python
linearDownscale(dfGCAM_energyRecategorized,dfEmissions,'Emissions_Recategorized',baseYear)
linearDownscale(dfGCAM_energyLinear,dfEmissions,'Emissions_Original',baseYear)
```

### Convergence Downscaling


>Convergence downscaling: An alternative to proportional downscaling is to assume some level of convergence to an average regional value, which tends to assure that the local out- come is dependent on the pathway of the larger unit. For some types of data (e.g., income), there are good reasons to assume that some form of convergence within larger global regions is likely to occur (see Ref 18 for a discussion of the method). This assumption applies especially to cases where large differ- ences between units within a region exist and proportional downscaling results in unlikely outcomes. The rate of convergence can be influenced by choosing a different convergence year. The scenario’s convergence characteristics at the macro-regional level can be used to guide the strength of the convergence assumptions for the downscaling (providing internal plausibility and consistency across different spatial scales).

– van Vuuren, D. P., Smith, S. J., & Riahi, K. (2010). Downscaling socioeconomic and emissions scenarios for global environmental change research: a review. Wiley Interdisciplinary Reviews: Climate Change, 1(3), 393–404. https://doi.org/10.1002/wcc.50


The convergence method mostly relies on the IPAT formula, which require scenario-specific projections of population and GDP change. In this case, that means we have to match the projections with the scenario used in GCAM.


>The choice of convergence year reflects the rate at which economic and energy systems converge toward similar structures within each native model region. Accordingly, the SSP1 and SSP5 scenarios are assigned relatively near-term convergence years of 2125, while SSP3 and SSP4 scenarios are assigned 2200, and SSP2 is assigned an intermediate value of 2150.

– Gidden, M. J., Riahi, K., Smith, S. J., Fujimori, S., Luderer, G., Kriegler, E., … Takahashi, K. (2019). Global emissions pathways under different socioeconomic scenarios for use in CMIP6: a dataset of harmonized emissions trajectories through the end of the century. Geoscientific Model Development, 12(4), 1443–1475. https://doi.org/10.5194/gmd-12-1443-2019


#### Export Base and Convergence Year GDP and Population

Country by country (eurostat + OECD), regional (GCAM, eurostat sum, OECD sum)

```python

```

#### Energy Data Re-categorization

```python
dfEnergy = pd.read_csv('../data/interim/eurostatEnergy.csv', index_col=0, parse_dates=['year'])
```

```python
dfEnergy = dfEnergy.rename(columns={'siecName':'technology', 'year':'Year'})
dfEnergy = dfEnergy.query("~technology.isin(Year==@baseYear)")
```

Gidden et al. only use IPAT for emissions from the energy sector, not energy use directly. Where they used $E$ for emissions, we will use it for energy.

##### Glossary of Variables

|Variable|Meaning|
|--|--|
|$t$|Time (year)|
|$I$|Energy intensity (per GDP)|
|$E$|Energy use|
|$GDP$|Gross domestic product|
|$n$|Population|
|$\beta$|Growth rate of energy intensity|
|$t_i$|Base (initial) year|
|$t_f$|Convergence (final) year (not the final year of simulation)|
|$R$|Region|
|$c$|Country|
|$E^*$|Energy use not yet normalized to model region data (for numeric consistency)|

This will differ by scenario. Gidden et al. used 2125 for SSP1 and SSP5, 2150 for SSP2, and 2200 for SSP3 and SSP4.

```python
convergenceYear = 2100 # This needs to change based on the scenario and at least 2150 for my use case.
```

<!-- #region toc-hr-collapsed=true toc-nb-collapsed=true toc-hr-collapsed=true toc-nb-collapsed=true -->
#### GDP
<!-- #endregion -->

<!-- #region toc-hr-collapsed=true toc-nb-collapsed=true -->
##### Intensity
<!-- #endregion -->

$$ I_t = \frac{E_t}{GDP_t} $$

Needs to be calculated for the region in the convergence year and for each country in the base year. The convergence year's value is extrapolated from the last period (2090–2100). Gidden et al. use exponential extrapolation for values with positive trend (including GDP itself) and a linear approach for those with a negative one.

```python
dfEnergy.query("geo == 'NL'")
```

```python
dfI_base = dfEnergy.query("Year.dt.year==@baseYear").drop(columns=['unit','value']).reset_index(drop=True)
```

Country-level (from external data)

```python
dfEnergy
```

```python
dfI_base['base_EnergyGDP'] = ( dfEnergy.query("Year.dt.year==@baseYear").set_index(['geo'])['value'] / dfGDP.query("Year==@baseYear").set_index('geo')['value'] ).reset_index()['value']
```

```python
dfI_base.query("geo=='NL'")
```

###### Special Cases

In the case that a country has a zero value in the base year, the following calculation is trivial; the growth will be zero and there will be no change in its energy consumption profile.

Gidden et al. (2019) set all regions with a zero value to one-third of the minimum non-zero value in that region (see description on [GitHub](https://github.com/iiasa/emissions_downscaling/wiki/Downscaling-Methodology#adjustments-to-zero-base-year-emissions-intensities)).

However, as of writing, no countries produce electricity from hydrogen, and Spain is the only country that uses solar thermal. These remaining zero values will be filled with a very small number, $1\times10^{-16}$.

There are also countries–resource combinations that do not appear in the Eurostat dataset at all, so they will need to be added back in.

```python
dfI_baseZeroCaseFix = dfI_base.copy()
# Subset only those countries with a zero
dfI_baseZeroCaseFix = dfI_baseZeroCaseFix[dfI_baseZeroCaseFix['base_EnergyGDP'] != 0].drop(columns='geo')
# Replace zeros with one-third the minimum of each technology set (within each year)
dfI_baseZeroCaseFix = dfI_baseZeroCaseFix.groupby(['Year','technology']).min() / 3
dfI_baseZeroCaseFix
```

```python
dfI_baseZeroCase = dfI_base[dfI_base['base_EnergyGDP'] == 0].reset_index().set_index(['Year','technology']) # Reset first to maintain the original index
dfI_baseZeroCase.head()
```

```python
dfI_baseZeroCase = dfI_baseZeroCase.drop(columns='base_EnergyGDP').join(dfI_baseZeroCaseFix)
dfI_baseZeroCase.head()
```

```python
# Fill the remaining zeros with 1e-16
dfI_baseZeroCase.loc[(dfI_baseZeroCase['base_EnergyGDP'] == 0) | (dfI_baseZeroCase['base_EnergyGDP'].isna()),'base_EnergyGDP'] = 1e-16
dfI_baseZeroCase.head()
```

```python
dfI_base.loc[dfI_baseZeroCase['index'],'base_EnergyGDP'] = dfI_baseZeroCase.set_index('index')['base_EnergyGDP']
```

```python
dfI_base.loc[dfI_baseZeroCase['index']].head()
```

```python
# Add back the countries–resource combinations that do not exist
recategorizedResources = ['Coal', 'Biomass', 'Natural gas', 'Nuclear', 'Oil', 'Hydro', 'Geothermal', 'Wind', 'Solar Thermal', 'Solar PV', 'Hydrogen']

for country in dfI_base['geo'].unique():
    for resource in recategorizedResources:
        if ~pd.Series(resource).isin(dfI_base.query("geo==@country & Year==@baseYear")['technology'].unique())[0]:
            dfI_base = dfI_base.append(
                pd.DataFrame({'geo':[country],'Year':[pd.to_datetime(baseYear, format='%Y')],'technology':[resource],'base_EnergyGDP':[1e-16]})
            )
            
dfI_base = dfI_base.reset_index(drop=True)
```

Regional from GCAM

```python
dfGCAM_energyRecategorized.query("Year == @convergenceYear")
```

```python
dfI_final = dfGCAM_energyRecategorized.query("Year == @convergenceYear").copy().set_index('technology').drop(columns=['Units','region','Year'])
```

```python
dfI_final = dfI_final.rename(columns={'value':'convergence_EnergyGDP'})
```

```python
dfI_final['convergence_EnergyGDP'] = (dfGCAM_energyRecategorized.query("Year==@convergenceYear")['value'].values / dfGCAM_GDP.query("Year==@convergenceYear")['value'].values)
```

```python
dfI_final
```

```python
dfI_final['Year'] = convergenceYear
dfI_final['geo'] = 'EU-15'
```

The individual countries' usage intensities are meant to converge to the regional value by the convergence date. It is good for verification and communication to show this step graphically.

```python
dfI_base.groupby(['technology'])['base_EnergyGDP'].mean()
```

```python
dfI_final
```

```python
dfI_compare = dfI_final.drop(columns=['scenario','Year','geo']).join(dfI_base.groupby(['technology'])['base_EnergyGDP'].mean())
dfI_compare = dfI_compare.reset_index().melt(['technology'])
dfI_compare
```

```python
chart = alt.Chart(
    dfI_compare.query("variable.str.contains('GDP')") # For robustness in case the Population section is already run
).mark_bar().encode(
    x = alt.X('variable:N', axis=alt.Axis(title=None, labels=False)),
    y = alt.Y('value:Q', axis=alt.Axis(title=['Energy Intensity Relative to GDP','(EJ/million USD (1990))'], format='e')),
    color=alt.Color('variable:N', legend=alt.Legend(title="Year"))
).facet(
    column = alt.Column('technology:N', title='Resource Type', header=alt.Header(labelOrient='bottom'))
)
chart
# Vega titles can be broken into multi-lines by making them lists: https://github.com/vega/vega/issues/488#issuecomment-534065749
```

```python
chart.save('../reports/figures/baseVSconvergence_EnergyGDP.pdf')
# If it is desirable to match the font in this plot with that of a LaTeX document, use the SVG package workflow.
```

It is interesting to see how the initial and convergence energy intensities for each resource type will be. In this reference scenario, only geothermal, hydrogen, and solar thermal intensities rise. All others, including hydro, wind, and solar PV, decrease.

One artifact for this calculation could be the disproportionate distribution of these intensities. For example, few EU-15 countries actually generate hydro electricity. When each country's value is calculated and then aggregated via a mean, the sizes of each economy (in GDP) are weighed equally.

```python
dfI_base
```

```python
# dfI_base.unmelt().to_latex()
```

```python
chart = alt.Chart(dfI_base).mark_rect().encode(
    x=alt.X('technology:N', title='Resource Type'),
    y=alt.Y('geo:N', title="Country"),
    color=alt.Color('base_EnergyGDP:Q', legend=alt.Legend(format='.1e', title=['Energy','Intensity']), scale=alt.Scale(type='sqrt', scheme='purples')),
    facet=alt.Facet('year(Year):O', title=None, header=alt.Header(labels=False)) # Labels=False doesn't seem to work
).properties(
    height=300
)
chart
```

```python
chart.save('../reports/figures/base_EnergyGDP.pdf')
```

```python
# Try and normalize each group by technology
# alt.Chart(dfI_base).mark_rect().encode(
#     x='technology:N',
#     y='geo:N',
#     color='base_EnergyGDP:Q',
#     facet=alt.Facet('Year')
# )
# .transform_calculate(
#     some_magical_function_here,
#     groupby=["technology"]
# )

# Might need scipy to help normalize first
```

It's strange to see that Italy does not have data for biomass, coal, natural gas, and oil. The middle two are important aspects of its power grid.

We can also see that France's heavy reliance on nuclear power is substantial.


##### Growth Rate


$$ {\beta}_c = {\frac{I_{R,t_f}}{I_{c,t_i}}}^{\frac{1}{t_f-t_i}} $$


Calculate

```python
dfI_final['convergence_EnergyGDP'] 
```

```python
dfI_base.set_index(['geo','technology']).drop(columns='Year')['base_EnergyGDP']
```

```python
beta = (dfI_final['convergence_EnergyGDP'] / dfI_base.set_index(['geo','technology']).drop(columns='Year')['base_EnergyGDP'])**(1/(convergenceYear-baseYear))
beta = beta.reset_index().rename(columns={0:'beta'})
beta = beta.set_index(['geo','technology'])
beta
```

```python
beta.reset_index().query("technology == 'Biomass' & geo == 'NL'")
```

```python
chart = alt.Chart(beta.reset_index()).mark_rect().encode(
    x=alt.X('technology:N', title='Resource Type'),
    y=alt.Y('geo:N', title="Country"),
    color=alt.Color('beta:Q', legend=alt.Legend(format='%'), scale={'domainMid':1, 'reverse':True})
).properties(
    height=300
)
chart

# blueorange
```

```python
chart.save('../reports/figures/beta_EnergyGDP.pdf')
```

<!-- #region toc-hr-collapsed=true toc-nb-collapsed=true -->
##### Interim Intensities

These values are based on the growth rate and assume the exponential growth pattern imposed by the previous step.

$$ I_{c,t} = {\beta}_c I_{c,t-1}$$
<!-- #endregion -->

We want to calculate and append, year by year, starting from the base year.

```python
dfI_base
```

```python
dfI_base.query("geo=='NL'")
```

```python
beta.query("geo=='NL'")
```

```python
(dfI_base.set_index(['geo','technology'])['base_EnergyGDP'] * (beta['beta'] ** 2)).to_frame().query("geo=='NL'")
```

```python
dfI = dfI_base.copy().rename(columns={'base_EnergyGDP':'I_ct_EnergyGDP'}).set_index(['geo','technology'])

for year in range(baseYear+(5-baseYear%5), convergenceYear-(convergenceYear%5)+1,5): # Increments by 5 until one past the last year the 5-floor of the convergence year
    # Create a new smaller DataFrame to track the looped year
    dfI_last = dfI_base.copy().rename(columns={'base_EnergyGDP':'I_ct_EnergyGDP'}).set_index(['geo','technology'])
    # Determine the change from the base year using the growth rate
    dfI_last['I_ct_EnergyGDP'] = dfI_base.set_index(['geo','technology'])['base_EnergyGDP'] * (beta['beta'] ** (year - baseYear))
    dfI_last = dfI_last.reset_index()
    dfI_last['Year'] = pd.to_datetime(year, format='%Y')
    dfI = dfI.append(dfI_last.set_index(['geo','technology']))

dfI = dfI.reset_index()
dfI
```

```python
dfI.query("geo=='DE' & technology=='Biomass'")['I_ct_EnergyGDP'].plot()
```

```python
dfI
```

```python
# Allow user to click on a country in the legend and highlight it in the plot
selection = alt.selection_multi(fields=['geo'], bind='legend')

chart = alt.Chart(dfI).mark_line().encode(
    x='Year:T',
    y=alt.Y('I_ct_EnergyGDP:Q', title=None, axis=alt.Axis(format='.0e')),
    color='geo:N',
    opacity = alt.condition(selection, alt.value(0.6), alt.value(0.2))
).properties(
    width=120,
    height=70
).facet(
    facet=alt.Facet('technology:N', title='Resource Type'),
    columns=3
).resolve_scale(y='independent').add_selection(selection) # Make this not independent and normalize!
chart
```

```python
chart.save('../reports/figures/interimIntensity_EnergyGDP.pdf')
```

##### Interim Energy Values Based on Interim Intensity and GDP

This will find the time series of energy values.

$$ {E_{c,t}^*} = I_{c,t} GDP_{c,t}$$

```python
dfData_convergenceEnergyGDP = dfI.copy().drop(columns='I_ct_EnergyGDP')
```

The country-level GDP data for each relevant year must come from another model. Gidden et al. (2019) approach took model projections from an OECD SSPs model. This data reports GDP as PPP (purchasing power parity) and in billions of 2005 USD. It also uses different country names than we do.

```python
dfIIASA_GDP = pd.read_csv('../data/external/iiasa_gdp.csv')
dfIIASA_GDP.head()
```

```python
countryMapIIASA = {'BEL':'Belgium','GRC':'Greece','PRT':'Portugal','ESP':'Spain','LUX':'Luxembourg','FRA':'France','DNK':'Denmark','DEU':'Germany','ITA':'Italy','NLD':'Netherlands','FIN':'Finland','AUT':'Austria','SWE':'Sweden','IRL':'Ireland','GBR':'United Kingdom'}
countryMapIIASA = {code.lower(): country for code, country in countryMapIIASA.items()}
```

```python
dfIIASA_GDP = dfIIASA_GDP.rename(columns={'iso':'geo'}).query("geo.isin(@countryMapIIASA.keys())").replace(countryMapIIASA)
dfIIASA_GDP.head()
```

```python
dfIIASA_GDP = dfIIASA_GDP.drop(columns='model').melt(id_vars=['geo','scenario','variable','unit'], var_name='Year')
dfIIASA_GDP['Year'] = dfIIASA_GDP['Year'].str.lstrip('X').astype(int)
dfIIASA_GDP['Year'] = pd.to_datetime(dfIIASA_GDP['Year'], format='%Y')
```

```python
dfIIASA_GDP['value'] = dfIIASA_GDP['value'] / 1.50 * 1000
dfIIASA_GDP['unit'] = 'MUSD_1990'
dfIIASA_GDP.head()
```

```python
countryMap = {'BE':'Belgium','EL':'Greece','PT':'Portugal','ES':'Spain','LU':'Luxembourg','FR':'France','DK':'Denmark','DE':'Germany','IT':'Italy','NL':'Netherlands','FI':'Finland','AT':'Austria','SE':'Sweden','IE':'Ireland','UK':'United Kingdom'}
countryMapInv = {v: k for k, v in countryMap.items()}
```

```python
dfIIASA_GDP = dfIIASA_GDP.replace(countryMapInv)
```

```python
dfI.set_index(['geo','Year','technology'])['I_ct_EnergyGDP'] * dfIIASA_GDP.query("Year.isin(@dfI['Year']) & scenario=='SSP2'").set_index(['geo','Year','scenario'])['value']
```

```python
dfData_convergenceEnergyGDP = dfData_convergenceEnergyGDP.set_index(['geo','Year','technology']).join(
    (
        dfI.set_index(['geo','Year','technology'])['I_ct_EnergyGDP'] * \
         dfIIASA_GDP.query("Year.isin(@dfI['Year']) & scenario=='SSP2'").set_index(['geo','Year','scenario'])['value']
    ).to_frame()
)
dfData_convergenceEnergyGDP = dfData_convergenceEnergyGDP.rename(columns={0:'E_ct*'}).reset_index()
dfData_convergenceEnergyGDP['Year'] = pd.to_datetime(dfData_convergenceEnergyGDP['Year'],format='%Y')
dfData_convergenceEnergyGDP.head()
```

Let's quickly look at (semantically verify) if this calculation proceeded as we expected.

```python
dfI_base.query("geo=='UK' & technology=='Coal'")
```

```python
dfI_final.query("technology=='Coal'")
```

```python
dfI.set_index(['geo','Year','technology']).query("geo=='UK' & technology=='Coal'")['I_ct_EnergyGDP'].plot()
```

```python
dfEnergy
```

```python
# What is the narrative purpose of this plot?
# Highlight that some countries use some form of energy and others don't?

# Allow user to click on a country in the legend and highlight it in the plot
selection = alt.selection_multi(fields=['geo'], bind='legend')

chart = alt.Chart(dfEnergy).mark_bar().encode(
    x=alt.X('geo:N', title='Country'),
    y=alt.Y('value:Q', axis=alt.Axis(format='.2f'), title='Energy (EJ)'),
    color=alt.Color('geo:N',title='Country'),
    opacity = alt.condition(selection, alt.value(1), alt.value(0.2))
).properties(
    width=130,
    height=70
).facet(
    facet=alt.Facet('technology:N',title='Resource Type'),
    columns=3
).resolve_scale(y='independent').add_selection(selection)
chart
```

```python
chart.save('../reports/figures/baseEnergyConsumption.pdf')
```

```python
# Allow user to click on a country in the legend and highlight it in the plot
selection = alt.selection_multi(fields=['geo'], bind='legend')

chart = alt.Chart(dfData_convergenceEnergyGDP).mark_line().encode(
    x='Year:T',
    y=alt.Y('E_ct*:Q',axis=alt.Axis(format=',.2f'), title='E_ct* (EJ)'),
    color=alt.Color('geo:N',title='Country'),
    opacity=alt.condition(selection, alt.value(1), alt.value(0.2))
).properties(
    width=150,
    height=70
).facet(
    facet=alt.Facet('technology:N',title='Resource Type'),
    columns=3
).resolve_scale(y='independent').add_selection(selection)
chart
```

```python
chart.save('../reports/figures/interimEnergy_EnergyGDP.pdf')
```

##### Normalize Interim Time Series by Regional (Model) Data

$$ {E_{c,t}} = \frac{E_{R,t}}{\sum_{c' \epsilon R}{E_{c',t}^*}} {E_{c,t}^*} $$


This last step normalizes the external dataset we introduced to the original model dataset we used 'to guarantee consistency between the spatial resolutions' (Gidden et al., 2019).

```python
dfData_convergenceEnergyGDP
```

```python
dfData_convergenceEnergyGDP.groupby(['Year','technology','geo','scenario'])['E_ct*'].sum()
```

**Here, the scenario need to match!!!** Units aren't an issue though.

```python
dfGCAM_energyRecategorized
```

```python
dfGCAM_energyRecategorized.query("Year.isin(@dfData_convergenceEnergyGDP['Year'])").set_index(['Year','technology','Units'])['value'] 
```

```python
dfData_convergenceEnergyGDP = dfData_convergenceEnergyGDP.set_index(['geo','Year','technology','scenario']).join(
    (
        dfGCAM_energyRecategorized.query("Year.isin(@dfData_convergenceEnergyGDP['Year'])").set_index(['Year','technology','Units'])['value'] * \
        dfData_convergenceEnergyGDP.set_index(['Year','technology','geo','scenario'])['E_ct*'] / \
        dfData_convergenceEnergyGDP.groupby(['Year','technology','scenario'])['E_ct*'].sum()
    ).to_frame().rename(columns={0:'E_ct'})
).reset_index()
dfData_convergenceEnergyGDP
```

```python
# Allow user to click on a country in the legend and highlight it in the plot
selection = alt.selection_multi(fields=['geo'], bind='legend')

chart = alt.Chart(dfData_convergenceEnergyGDP).mark_line().encode(
    x='Year:T',
    y=alt.Y('E_ct:Q',axis=alt.Axis(format=',.2f'), title='E_ct (EJ)'),
    color=alt.Color('geo:N',title='Country'),
    opacity=alt.condition(selection, alt.value(1), alt.value(0.2))
).properties(
    width=150,
    height=70
).facet(
    facet=alt.Facet('technology:N',title='Resource Type'),
    columns=3
).resolve_scale(y='independent').add_selection(selection)
chart
```

```python
chart.save('../reports/figures/convergence_EnergyGDP.pdf')
```

```python
dfData_convergenceEnergyGDP = dfData_convergenceEnergyGDP.rename(columns={'E_ct':'value','geo':'region'})
```

```python
dfData_convergenceEnergyGDP.to_csv('../data/interim/convergenceEnergyGDP.csv')
```

```python
dfData_convergenceEnergyGDP.drop(columns='E_ct*').to_csv('../data/processed/convergenceEnergyGDP.csv')
```

#### Population

We will do the same thing but with population intensity. This will also build upon the [previous section](), so make sure to run the GDP convergence downscaling first if any errors arise below.

Note that the Eurostat population data is written in ones, but GCAM records in the thousands and OECD in millions. We will stick with thousands in this analysis.


##### Intensity


$$ I_t = \frac{E_t}{n_t} $$

Needs to be calculated for the region in the convergence year and for each country in the base year. The convergence year's value is extrapolated from the last period (2090–2100).


Country-level (from external data)

```python
dfI_base['base_EnergyPopulation'] = ( dfEnergy.query("Year==@baseYear").set_index(['geo'])['value'] / dfPopulation.query("Year==@baseYear").set_index('geo')['value'] * 1000).reset_index()['value']
```

```python
dfI_base.query("geo=='NL'")
```

###### Special Cases

In the case that a country has a zero value in the base year, the following calculation is trivial; the growth will be zero and there will be no change in its energy consumption profile.

Gidden et al. (2019) set all regions with a zero value to one-third of the minimum non-zero value in that region (see description on [GitHub](https://github.com/iiasa/emissions_downscaling/wiki/Downscaling-Methodology#adjustments-to-zero-base-year-emissions-intensities)).

However, as of writing, no countries produce electricity from hydrogen, and Spain is the only country that uses solar thermal. These remaining zero values will be filled with a very small number, $1\times10^{-16}$.

```python
dfI_baseZeroCaseFix = dfI_base.copy().drop(columns='base_EnergyGDP')
# Subset only those countries with a zero
dfI_baseZeroCaseFix = dfI_baseZeroCaseFix[dfI_baseZeroCaseFix['base_EnergyPopulation'] != 0].drop(columns='geo')
# Replace zeros with one-third the minimum of each technology set (within each year)
dfI_baseZeroCaseFix = dfI_baseZeroCaseFix.groupby(['Year','technology']).min() / 3
dfI_baseZeroCaseFix
```

```python
dfI_base[(dfI_base['base_EnergyPopulation'] == 0)|(dfI_base['base_EnergyPopulation'].isna())]
```

```python
dfI_baseZeroCase = dfI_base[(dfI_base['base_EnergyPopulation'] == 0)|(dfI_base['base_EnergyPopulation'].isna())].drop(columns='base_EnergyGDP').reset_index().set_index(['Year','technology']) # Reset first to maintain the original index
dfI_baseZeroCase.head()
```

```python
dfI_baseZeroCase = dfI_baseZeroCase.drop(columns='base_EnergyPopulation').join(dfI_baseZeroCaseFix)
dfI_baseZeroCase.head()
```

```python
# Fill the remaining zeros with 1e-16
dfI_baseZeroCase.loc[(dfI_baseZeroCase['base_EnergyPopulation'] == 0)|(dfI_baseZeroCase['base_EnergyPopulation'].isna()),'base_EnergyPopulation'] = 1e-16
dfI_baseZeroCase.head()
```

```python
dfI_base.loc[dfI_baseZeroCase['index'],'base_EnergyPopulation'] = dfI_baseZeroCase.set_index('index')['base_EnergyPopulation']
```

```python
dfI_base.loc[dfI_baseZeroCase['index']].head()
```

Check if there are any zeros or nans left.

```python
dfI_base[(dfI_base['base_EnergyPopulation'] == 0)|(dfI_base['base_EnergyPopulation'].isna())]
```

Regional from GCAM

```python
dfGCAM_population.query("Year==@convergenceYear")['value'].values
```

The population values are in thousands.

```python
dfGCAM_energyRecategorized.query("Year==@convergenceYear").set_index('technology')['value']
```

```python
dfI_final['convergence_EnergyPopulation'] = dfGCAM_energyRecategorized.query("Year==@convergenceYear").set_index('technology')['value'] / dfGCAM_population.query("Year==@convergenceYear")['value'].values
```

```python
dfI_final
```

The individual countries' usage intensities are meant to converge to the regional value by the convergence date. It is good for verification and communication to show this step graphically.

```python
dfI_compare
```

```python
dfI_compare = dfI_compare.append([
    dfI_base.groupby('technology')['base_EnergyPopulation'].mean().to_frame().reset_index().melt(['technology']),
    dfI_final['convergence_EnergyPopulation'].reset_index().melt('technology')
])
dfI_compare = dfI_compare.reset_index(drop=True)
dfI_compare
```

```python
chart = alt.Chart(
    dfI_compare.query("variable.str.contains('Population')") # Filter out GDP
).mark_bar().encode(
    x = alt.X('variable:N', axis=alt.Axis(title=None, labels=False)),
    y = alt.Y('value:Q', axis=alt.Axis(title=['Energy Intensity Relative to Population','(EJ/thousand))'], format='e')),
    color=alt.Color('variable:N', legend=alt.Legend(title="Year"))
).facet(
    column = alt.Column('technology:N', title='Resource Type', header=alt.Header(labelOrient='bottom'))
)
chart
# Vega titles can be broken into multi-lines by making them lists: https://github.com/vega/vega/issues/488#issuecomment-534065749
```

```python
chart.save('../reports/figures/baseVSconvergence_EnergyPopulation.pdf')
# If it is desirable to match the font in this plot with that of a LaTeX document, use the SVG package workflow.
```

It is interesting to see how the initial and convergence energy intensities for each resource type will be. In this reference scenario, only geothermal, hydrogen, and solar thermal intensities rise. All others, including hydro, wind, and solar PV, decrease.

One artifact for this calculation could be the disproportionate distribution of these intensities. For example, few EU-15 countries actually generate hydro electricity. When each country's value is calculated and then aggregated via a mean, the sizes of each economy (in GDP) are weighed equally.

```python
dfI_base
```

```python
# dfI_base.unmelt().to_latex()
```

```python
chart = alt.Chart(dfI_base).mark_rect().encode(
    x=alt.X('technology:N', title='Resource Type'),
    y=alt.Y('geo:N', title="Country"),
    color=alt.Color('base_EnergyPopulation:Q', legend=alt.Legend(format='.1e', title=['Energy','Intensity']), scale=alt.Scale(type='sqrt', scheme='purples')),
    facet=alt.Facet('year(Year):O', title=None,header=alt.Header(labels=False))
)
chart
```

```python
chart.save('../reports/figures/base_EnergyPopulation.pdf')
```

```python
# Try and normalize each group by technology
# alt.Chart(dfI_base).mark_rect().encode(
#     x='technology:N',
#     y='geo:N',
#     color='base_EnergyGDP:Q',
#     facet=alt.Facet('Year')
# )
# .transform_calculate(
#     some_magical_function_here,
#     groupby=["technology"]
# )

# Might need scipy to help normalize first
```

Like previously, Italy does not have data for biomass, coal, natural gas, and oil. France's heavy reliance on nuclear power remains, but now Sweden's intensity for hydro and nuclear is high.

```python
dfI_base.query("geo=='SE'")
```

##### Growth Rate


$$ {\beta}_c = {\frac{I_{R,t_f}}{I_{c,t_i}}}^{\frac{1}{t_f-t_i}} $$


Calculate

```python
beta = (dfI_final['convergence_EnergyPopulation'] / dfI_base.set_index(['geo','technology']).drop(columns='Year')['base_EnergyPopulation'])**(1/(convergenceYear-baseYear))
beta = beta.reset_index().rename(columns={0:'beta'})
beta = beta.set_index(['geo','technology'])
beta
```

```python
beta.reset_index().query("technology == 'Biomass' & geo == 'NL'")
```

```python
chart = alt.Chart(beta.reset_index()).mark_rect().encode(
    x=alt.X('technology:N', title='Resource Type'),
    y=alt.Y('geo:N', title="Country"),
    color=alt.Color('beta:Q', legend=alt.Legend(format='%'), scale={'domainMid':1, 'reverse':True})
).properties(
    height=300
)
chart
```

```python
chart.save('../reports/figures/beta_EnergyPopulation.pdf')
```

##### Interim Intensities

These values are based on the growth rate and assume the exponential growth pattern imposed by the previous step.

$$ I_{c,t} = {\beta}_c I_{c,t-1}$$


We want to calculate and append, year by year, starting from the base year.

```python
# Calculates every year's value with the first year as the baseline
dfI['YearInt'] = dfI['Year'].dt.year
dfI = dfI.set_index(['geo','technology'])
dfI = dfI.join(dfI_base.set_index(['geo','technology'])['base_EnergyPopulation']).rename(columns={'base_EnergyPopulation':'I_ct_EnergyPopulation'})
dfI['I_ct_EnergyPopulation'] = dfI['I_ct_EnergyPopulation'] * beta['beta'] ** (dfI['YearInt']- baseYear)
dfI = dfI.drop(columns=['YearInt']).reset_index()
dfI
```

```python
dfI.query("geo=='UK' & technology=='Biomass'")
```

```python
dfI.query("geo=='NL' & technology=='Biomass'")['I_ct_EnergyPopulation'].plot()
```

```python
# Allow user to click on a country in the legend and highlight it in the plot
selection = alt.selection_multi(fields=['geo'], bind='legend')

chart = alt.Chart(dfI).mark_line().encode(
    x='Year:T',
    y=alt.Y('I_ct_EnergyPopulation:Q', title=None, axis=alt.Axis(format='.0e')),
    color='geo:N',
    opacity = alt.condition(selection, alt.value(0.6), alt.value(0.2))
).properties(
    width=120,
    height=70
).facet(
    facet=alt.Facet('technology:N', title='Resource Type'),
    columns=3
).resolve_scale(y='independent').add_selection(selection) # Make this not independent and normalize!
chart
```

```python
chart.save('../reports/figures/interimIntensity_EnergyPopulation.pdf')
```

##### Interim Energy Values Based on Interim Intensity and GDP

This will find the time series of energy values.

$$ {E_{c,t}^*} = I_{c,t} n_{c,t} $$

```python
dfData_convergenceEnergyPopulation = dfI.copy().drop(columns='I_ct_EnergyGDP')
```

The country-level GDP data for each relevant year must come from another model. Gidden et al. (2019) approach took model projections from an OECD SSPs model. This data reports GDP as PPP (purchasing power parity) and in billions of 2005 USD. It also uses different country names than we do.

```python
dfIIASA_population = pd.read_csv('../data/external/iiasa_population.csv')
dfIIASA_population.head()
```

```python
dfIIASA_population = dfIIASA_population.rename(columns={'iso':'geo'}).query("geo.isin(@countryMapIIASA.keys())").replace(countryMapIIASA)
dfIIASA_population.head()
```

```python
dfIIASA_population = dfIIASA_population.drop(columns='model').melt(id_vars=['geo','scenario','variable','unit'], var_name='Year')
dfIIASA_population['Year'] = dfIIASA_population['Year'].str.lstrip('X').astype(int)
dfIIASA_population['Year'] = pd.to_datetime(dfIIASA_population['Year'], format='%Y')
```

```python
dfIIASA_population['value'] = dfIIASA_population['value'] * 1000
dfIIASA_population['unit'] = 'thousands'
dfIIASA_population.head()
```

```python
dfIIASA_population = dfIIASA_population.replace(countryMapInv)
```

```python
dfI.set_index(['geo','Year','technology'])['I_ct_EnergyPopulation'] * dfIIASA_population.query("Year.isin(@dfI['Year']) & scenario=='SSP2'").set_index(['geo','Year','scenario'])['value']
```

```python
dfData_convergenceEnergyPopulation = dfData_convergenceEnergyPopulation.set_index(['geo','Year','technology']).join(
    (
        dfI.set_index(['geo','Year','technology'])['I_ct_EnergyPopulation'] * \
         dfIIASA_population.query("Year.isin(@dfI['Year']) & scenario=='SSP2'").set_index(['geo','Year','scenario'])['value']
    ).to_frame()
)
dfData_convergenceEnergyPopulation = dfData_convergenceEnergyPopulation.rename(columns={0:'E_ct*'}).reset_index()
dfData_convergenceEnergyPopulation['Year'] = pd.to_datetime(dfData_convergenceEnergyPopulation['Year'],format='%Y')
dfData_convergenceEnergyPopulation.head()
```

```python
# Allow user to click on a country in the legend and highlight it in the plot
selection = alt.selection_multi(fields=['geo'], bind='legend')

chart = alt.Chart(dfData_convergenceEnergyPopulation).mark_line().encode(
    x='Year:T',
    y=alt.Y('E_ct*:Q',axis=alt.Axis(format=',.2f'), title='E_ct* (EJ)'),
    color=alt.Color('geo:N',title='Country'),
    opacity=alt.condition(selection, alt.value(1), alt.value(0.2))
).properties(
    width=150,
    height=70
).facet(
    facet=alt.Facet('technology:N',title='Resource Type'),
    columns=3
).resolve_scale(y='independent').add_selection(selection)
chart
```

```python
chart.save('../reports/figures/interimEnergy_EnergyPopulation.pdf')
```

##### Normalize Interim Time Series by Regional (Model) Data

$$ {E_{c,t}} = \frac{E_{R,t}}{\sum_{c' \epsilon R}{E_{c',t}^*}} {E_{c,t}^*} $$


This last step normalizes the external dataset we introduced to the original model dataset we used 'to guarantee consistency between the spatial resolutions' (Gidden et al., 2019).

```python
dfData_convergenceEnergyPopulation
```

```python
dfData_convergenceEnergyPopulation.groupby(['Year','technology','geo','scenario'])['E_ct*'].sum()
```

**Here, the scenario need to match!!!** Units aren't an issue though.

```python
dfGCAM_energyRecategorized
```

```python
dfGCAM_energyRecategorized.query("Year.isin(@dfData_convergenceEnergyPopulation['Year'])").set_index(['Year','technology','Units'])['value'] 
```

```python
dfData_convergenceEnergyPopulation = dfData_convergenceEnergyPopulation.set_index(['geo','Year','technology','scenario']).join(
    (
        dfGCAM_energyRecategorized.query("Year.isin(@dfData_convergenceEnergyPopulation['Year'])").set_index(['Year','technology','Units'])['value'] * \
        dfData_convergenceEnergyPopulation.set_index(['Year','technology','geo','scenario'])['E_ct*'] / \
        dfData_convergenceEnergyPopulation.groupby(['Year','technology','scenario'])['E_ct*'].sum()
    ).to_frame().rename(columns={0:'E_ct'})
).reset_index()
dfData_convergenceEnergyPopulation
```

```python
# Allow user to click on a country in the legend and highlight it in the plot
selection = alt.selection_multi(fields=['geo'], bind='legend')

chart = alt.Chart(dfData_convergenceEnergyPopulation).mark_line().encode(
    x='Year:T',
    y=alt.Y('E_ct:Q',axis=alt.Axis(format=',.2f'), title='E_ct (EJ)'),
    color=alt.Color('geo:N',title='Country'),
    opacity=alt.condition(selection, alt.value(1), alt.value(0.2))
).properties(
    width=150,
    height=70
).facet(
    facet=alt.Facet('technology:N',title='Resource Type'),
    columns=3
).resolve_scale(y='independent').add_selection(selection)
chart
```

```python
chart.save('../reports/figures/convergence_EnergyPopulation.pdf')
```

```python
dfData_convergenceEnergyPopulation = dfData_convergenceEnergyPopulation.rename(columns={'E_ct':'value','geo':'region'})
```

```python
dfData_convergenceEnergyPopulation.head()
```

```python
dfData_convergenceEnergyPopulation.drop(columns=['I_ct_EnergyPopulation','E_ct*']).to_csv('../data/processed/convergenceEnergyPopulation.csv')
```

### External-Input-Based Downscaling

```python

```
