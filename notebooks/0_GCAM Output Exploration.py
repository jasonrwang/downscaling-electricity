#
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,md
#     text_representation:
#       extension: .md
#       format_name: markdown
#       format_version: '1.2'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: 'Python 3.8.1 64-bit (''3.8.1'': pyenv)'
#     language: python
#     name: python38164bit381pyenvb3df2019228041fdaa1b32128074601a
# ---
#
# # 0 GCAM Output Exploration

import pandas as pd
import gcam_reader
from pathlib import Path # Pathlib is nicer to work with than os.path
import os.path # gcam_reader prefers os.path

# ## Connect to the GCAM Output Database
#
# This version of reference outputs is from GCAM 5.2 using the default reference configuration file.

dbpath = Path.home().joinpath("Local Projects/gcam-v5/output")
dbfile = "database_basexdb" # Folder name of output database
conn = gcam_reader.LocalDBConn(dbpath, dbfile) # Make connection object and prints list of scenarios

# ### Define Queries
#
# The database is very large, so we must dictate which specific outcomes we want.

# +
queryFile = Path("../data/raw/queries.xml")
with open(queryFile.resolve()) as queries:
    queries = gcam_reader.parse_batch_query(queries)

[q.title for q in queries]
# -

# ### Run and Save Queries
#
# `gcam_reader`'s `runQuery()` method turns a database into a Pandas dataframe, which will need to be stored into a useable format for further analysis.
#
# We will save the dataframe of interest into a CSV for downscaling in the next notebook.

for query in queries:
    dfData = conn.runQuery(query)
    dfData.head()
    dfData.to_csv("../data/interim/GCAM_" + query.title + ".csv")

# For some reason, the electricity data are not being extracted as expected. There are multiple entries for each technology and they need to be summed to match the visual interface in the GCAM ModelInterface.

dfData = conn.runQuery(queries[0])
dfData = dfData.groupby(['scenario','region','Year','technology','Units']).sum().reset_index()
dfData.to_csv("../data/interim/GCAM_" + queries[0].title + ".csv")

dfData.groupby(['scenario','region','Year','technology','Units']).sum().reset_index().query("region=='EU-15' & technology=='wind'")


