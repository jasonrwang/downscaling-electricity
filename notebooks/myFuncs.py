import pandas as pd
from pathlib import Path

def linearDownscale(dfParent, dfRelative, relativeName, baseYear):
    '''
    This function downscales model outputs in one function relative to some attribute.

    Input
        dfParent – Pandas DataFrame with year, country, and values as 'Year', 'region', and 'value'.
        dfRelative – Pandas DataFrame with year, country, and values columns as 'Year', 'geo', and 'value'.
        relativeName – a string that represents the variable downscaled relative to.
        baseYear – need to know the base year for the analysis.

    Output
        Saves a CSV file named with linear suffixed by relativeName.
        Returns the linearized values as a modified version of dfParent.
    '''

    # Find proportions for each country relative to the sum of the whole group.
    dfRelativeProportion = dfRelative.copy()
    dfRelativeProportion['value'] = dfRelative.set_index(['Year','geo'])['value'].values/ dfRelative.groupby(['Year'])['value'].transform('sum')
    dfRelativeProportion['unit'] = 'PR' # proportional
    
    # Save the relative proportion as an interim variable
    savePath = Path('../data/processed/linearProportions{}.csv'.format(relativeName))
    dfRelativeProportion.to_csv(savePath)
    print('Saved proportions successfully as {}.'.format(savePath))

    # Multiply the values in dfParent by this proportional value
    dfLinearized = pd.DataFrame()
    dfData_linearTemp = pd.DataFrame()
    for country in dfRelative['geo'].unique():
        dfData_linearTemp = dfParent.copy()
        dfData_linearTemp['region'] = country
        dfData_linearTemp['value'] = \
            dfParent['value'] * dfRelativeProportion.query("geo==@country & Year==@baseYear")['value'].values
        dfLinearized = dfLinearized.append(dfData_linearTemp)
    
    # Save the linearized data
    savePath = Path('../data/processed/linear{}.csv'.format(relativeName))
    dfLinearized.reset_index(drop=True).to_csv(savePath)
    print('Saved linear downscaled results successfully as {}.'.format(savePath))

    # Return true to signal that it completed properly
    return True
