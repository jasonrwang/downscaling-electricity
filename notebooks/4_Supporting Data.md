# 4 Supporting Data

This notebook is to collect, interpret, and produce visualizations for data that support the analysis.


```python
import pandas as pd
import altair as alt
```

```html
<style>
@import url('https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap');
</style>
```

```python
def my_theme():
    return {
        'config': {'range': {'category': {'scheme': 'category20'}},
                    'font': 'Source Sans Pro'}
    }

alt.themes.register('my_theme', my_theme)
alt.themes.enable('my_theme')
```

## Composition of Economy in EU

Data is from Ian Coleman's GitHub repository that scrapes the factbook into JSON data weekly. The historical data is updated from time to time as a large archive. The latest data is periodically updated and available directly from the repository. Here, I use the [2020-04-27 data](https://github.com/iancoleman/cia_world_factbook_api/blob/707f363e69faa25a773d8bc3c62edfe29217c5ec/data/factbook.json) from commit `707f363`.


```python
dfFactbook = pd.read_json('https://github.com/iancoleman/cia_world_factbook_api/blob/707f363e69faa25a773d8bc3c62edfe29217c5ec/data/factbook.json?raw=true')
```


```python
dfFactbook.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>countries</th>
      <th>metadata</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>world</th>
      <td>{'data': {'name': 'World', 'introduction': {'b...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>afghanistan</th>
      <td>{'data': {'name': 'Afghanistan', 'introduction...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>akrotiri</th>
      <td>{'data': {'name': 'Akrotiri', 'introduction': ...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>albania</th>
      <td>{'data': {'name': 'Albania', 'introduction': {...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>algeria</th>
      <td>{'data': {'name': 'Algeria', 'introduction': {...</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>



We are only interested in the EU-15 countries.


```python
countryMap = {'BE':'Belgium','EL':'Greece','PT':'Portugal','ES':'Spain','LU':'Luxembourg','FR':'France','DK':'Denmark','DE':'Germany','IT':'Italy','NL':'Netherlands','FI':'Finland','AT':'Austria','SE':'Sweden','IE':'Ireland','UK':'United Kingdom'}
```

The CIA Factbook data uses `united_kingdom`.


```python
countryMap.update({'UK':'United_Kingdom'})
```


```python
dfFactbook.index[dfFactbook.index.isin([i.lower() for i in countryMap.values()])]
```




    Index(['austria', 'belgium', 'denmark', 'finland', 'france', 'germany',
           'greece', 'ireland', 'italy', 'luxembourg', 'netherlands', 'portugal',
           'spain', 'sweden', 'united_kingdom'],
          dtype='object')




```python
dfFactbook_EU15 = dfFactbook.loc[dfFactbook.index.isin([i.lower() for i in countryMap.values()])]
```


```python
dfFactbook_EU15
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>countries</th>
      <th>metadata</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>austria</th>
      <td>{'data': {'name': 'Austria', 'introduction': {...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>belgium</th>
      <td>{'data': {'name': 'Belgium', 'introduction': {...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>denmark</th>
      <td>{'data': {'name': 'Denmark', 'introduction': {...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>finland</th>
      <td>{'data': {'name': 'Finland', 'introduction': {...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>france</th>
      <td>{'data': {'name': 'France', 'introduction': {'...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>germany</th>
      <td>{'data': {'name': 'Germany', 'introduction': {...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>greece</th>
      <td>{'data': {'name': 'Greece', 'introduction': {'...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>ireland</th>
      <td>{'data': {'name': 'Ireland', 'introduction': {...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>italy</th>
      <td>{'data': {'name': 'Italy', 'introduction': {'b...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>luxembourg</th>
      <td>{'data': {'name': 'Luxembourg', 'introduction'...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>netherlands</th>
      <td>{'data': {'name': 'Netherlands', 'introduction...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>portugal</th>
      <td>{'data': {'name': 'Portugal', 'introduction': ...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>spain</th>
      <td>{'data': {'name': 'Spain', 'introduction': {'b...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>sweden</th>
      <td>{'data': {'name': 'Sweden', 'introduction': {'...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>united_kingdom</th>
      <td>{'data': {'name': 'United Kingdom', 'introduct...</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>




```python
dfFactbook_EU15['countries'][1]['data']['economy']['gdp']['composition']['by_sector_of_origin']
```




    {'sectors': {'agriculture': {'value': 0.7000000000000001, 'units': '%'},
      'industry': {'value': 22.1, 'units': '%'},
      'services': {'value': 77.2, 'units': '%'}},
     'date': '2017'}




```python
# Declare new empty df
dfComposition_EU15 = pd.DataFrame()

# Extract the detailed info from each country
for country, data in dfFactbook_EU15['countries'].items():
    # Create temp var to act as indexing shortcut
    composition = data['data']['economy']['gdp']['composition']['by_sector_of_origin']
    
    appendRow = {
        'country':[country],
        'agriculture':[composition['sectors']['agriculture']['value']],
        'industry':[composition['sectors']['industry']['value']],
        'services':[composition['sectors']['services']['value']],
        'Year':[composition['date']]
    }
    
    dfComposition_EU15 = dfComposition_EU15.append(pd.DataFrame.from_dict(appendRow))
```


```python
dfComposition_EU15.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>country</th>
      <th>agriculture</th>
      <th>industry</th>
      <th>services</th>
      <th>Year</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>austria</td>
      <td>1.3</td>
      <td>28.4</td>
      <td>70.3</td>
      <td>2017</td>
    </tr>
    <tr>
      <th>0</th>
      <td>belgium</td>
      <td>0.7</td>
      <td>22.1</td>
      <td>77.2</td>
      <td>2017</td>
    </tr>
    <tr>
      <th>0</th>
      <td>denmark</td>
      <td>1.3</td>
      <td>22.9</td>
      <td>75.8</td>
      <td>2017</td>
    </tr>
    <tr>
      <th>0</th>
      <td>finland</td>
      <td>2.7</td>
      <td>28.2</td>
      <td>69.1</td>
      <td>2017</td>
    </tr>
    <tr>
      <th>0</th>
      <td>france</td>
      <td>1.7</td>
      <td>19.5</td>
      <td>78.8</td>
      <td>2017</td>
    </tr>
  </tbody>
</table>
</div>




```python
dfComposition_EU15['country'] = dfComposition_EU15['country'].str.title()
dfComposition_EU15.loc[dfComposition_EU15['country'] == 'United_Kingdom', 'country'] = 'United Kindgom'
```


```python
dfComposition_EU15.tail()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>country</th>
      <th>agriculture</th>
      <th>industry</th>
      <th>services</th>
      <th>Year</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Netherlands</td>
      <td>1.6</td>
      <td>17.9</td>
      <td>70.2</td>
      <td>2017</td>
    </tr>
    <tr>
      <th>0</th>
      <td>Portugal</td>
      <td>2.2</td>
      <td>22.1</td>
      <td>75.7</td>
      <td>2017</td>
    </tr>
    <tr>
      <th>0</th>
      <td>Spain</td>
      <td>2.6</td>
      <td>23.2</td>
      <td>74.2</td>
      <td>2017</td>
    </tr>
    <tr>
      <th>0</th>
      <td>Sweden</td>
      <td>1.6</td>
      <td>33.0</td>
      <td>65.4</td>
      <td>2017</td>
    </tr>
    <tr>
      <th>0</th>
      <td>United Kindgom</td>
      <td>0.7</td>
      <td>20.2</td>
      <td>79.2</td>
      <td>2017</td>
    </tr>
  </tbody>
</table>
</div>



For some reason, the Dutch data do not sum to 100. Here, we will use its 2013 values instead.


```python
dfComposition_EU15.loc[dfComposition_EU15['country']=='Netherlands'] = \
    pd.DataFrame.from_dict(
        {'country':['Netherlands'],
         'agriculture':[2.5],
         'industry':[24.9],
         'services':[72.6],
         'Year':[2013]}
    )
```


```python
dfComposition_EU15 = dfComposition_EU15.melt(id_vars=['country','Year'],var_name='Sector')
dfComposition_EU15 = dfComposition_EU15.rename(columns={'country':'Country'})
dfComposition_EU15['Sector'] = dfComposition_EU15['Sector'].str.title()
dfComposition_EU15['Units'] = '%'
```


```python
dfComposition_EU15
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Country</th>
      <th>Year</th>
      <th>Sector</th>
      <th>value</th>
      <th>Units</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Austria</td>
      <td>2017</td>
      <td>Agriculture</td>
      <td>1.3</td>
      <td>%</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Belgium</td>
      <td>2017</td>
      <td>Agriculture</td>
      <td>0.7</td>
      <td>%</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Denmark</td>
      <td>2017</td>
      <td>Agriculture</td>
      <td>1.3</td>
      <td>%</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Finland</td>
      <td>2017</td>
      <td>Agriculture</td>
      <td>2.7</td>
      <td>%</td>
    </tr>
    <tr>
      <th>4</th>
      <td>France</td>
      <td>2017</td>
      <td>Agriculture</td>
      <td>1.7</td>
      <td>%</td>
    </tr>
    <tr>
      <th>5</th>
      <td>Germany</td>
      <td>2017</td>
      <td>Agriculture</td>
      <td>0.7</td>
      <td>%</td>
    </tr>
    <tr>
      <th>6</th>
      <td>Greece</td>
      <td>2017</td>
      <td>Agriculture</td>
      <td>4.1</td>
      <td>%</td>
    </tr>
    <tr>
      <th>7</th>
      <td>Ireland</td>
      <td>2017</td>
      <td>Agriculture</td>
      <td>1.2</td>
      <td>%</td>
    </tr>
    <tr>
      <th>8</th>
      <td>Italy</td>
      <td>2017</td>
      <td>Agriculture</td>
      <td>2.1</td>
      <td>%</td>
    </tr>
    <tr>
      <th>9</th>
      <td>Luxembourg</td>
      <td>2017</td>
      <td>Agriculture</td>
      <td>0.3</td>
      <td>%</td>
    </tr>
    <tr>
      <th>10</th>
      <td>Netherlands</td>
      <td>2013</td>
      <td>Agriculture</td>
      <td>2.5</td>
      <td>%</td>
    </tr>
    <tr>
      <th>11</th>
      <td>Portugal</td>
      <td>2017</td>
      <td>Agriculture</td>
      <td>2.2</td>
      <td>%</td>
    </tr>
    <tr>
      <th>12</th>
      <td>Spain</td>
      <td>2017</td>
      <td>Agriculture</td>
      <td>2.6</td>
      <td>%</td>
    </tr>
    <tr>
      <th>13</th>
      <td>Sweden</td>
      <td>2017</td>
      <td>Agriculture</td>
      <td>1.6</td>
      <td>%</td>
    </tr>
    <tr>
      <th>14</th>
      <td>United Kindgom</td>
      <td>2017</td>
      <td>Agriculture</td>
      <td>0.7</td>
      <td>%</td>
    </tr>
    <tr>
      <th>15</th>
      <td>Austria</td>
      <td>2017</td>
      <td>Industry</td>
      <td>28.4</td>
      <td>%</td>
    </tr>
    <tr>
      <th>16</th>
      <td>Belgium</td>
      <td>2017</td>
      <td>Industry</td>
      <td>22.1</td>
      <td>%</td>
    </tr>
    <tr>
      <th>17</th>
      <td>Denmark</td>
      <td>2017</td>
      <td>Industry</td>
      <td>22.9</td>
      <td>%</td>
    </tr>
    <tr>
      <th>18</th>
      <td>Finland</td>
      <td>2017</td>
      <td>Industry</td>
      <td>28.2</td>
      <td>%</td>
    </tr>
    <tr>
      <th>19</th>
      <td>France</td>
      <td>2017</td>
      <td>Industry</td>
      <td>19.5</td>
      <td>%</td>
    </tr>
    <tr>
      <th>20</th>
      <td>Germany</td>
      <td>2017</td>
      <td>Industry</td>
      <td>30.7</td>
      <td>%</td>
    </tr>
    <tr>
      <th>21</th>
      <td>Greece</td>
      <td>2017</td>
      <td>Industry</td>
      <td>16.9</td>
      <td>%</td>
    </tr>
    <tr>
      <th>22</th>
      <td>Ireland</td>
      <td>2017</td>
      <td>Industry</td>
      <td>38.6</td>
      <td>%</td>
    </tr>
    <tr>
      <th>23</th>
      <td>Italy</td>
      <td>2017</td>
      <td>Industry</td>
      <td>23.9</td>
      <td>%</td>
    </tr>
    <tr>
      <th>24</th>
      <td>Luxembourg</td>
      <td>2017</td>
      <td>Industry</td>
      <td>12.8</td>
      <td>%</td>
    </tr>
    <tr>
      <th>25</th>
      <td>Netherlands</td>
      <td>2013</td>
      <td>Industry</td>
      <td>24.9</td>
      <td>%</td>
    </tr>
    <tr>
      <th>26</th>
      <td>Portugal</td>
      <td>2017</td>
      <td>Industry</td>
      <td>22.1</td>
      <td>%</td>
    </tr>
    <tr>
      <th>27</th>
      <td>Spain</td>
      <td>2017</td>
      <td>Industry</td>
      <td>23.2</td>
      <td>%</td>
    </tr>
    <tr>
      <th>28</th>
      <td>Sweden</td>
      <td>2017</td>
      <td>Industry</td>
      <td>33.0</td>
      <td>%</td>
    </tr>
    <tr>
      <th>29</th>
      <td>United Kindgom</td>
      <td>2017</td>
      <td>Industry</td>
      <td>20.2</td>
      <td>%</td>
    </tr>
    <tr>
      <th>30</th>
      <td>Austria</td>
      <td>2017</td>
      <td>Services</td>
      <td>70.3</td>
      <td>%</td>
    </tr>
    <tr>
      <th>31</th>
      <td>Belgium</td>
      <td>2017</td>
      <td>Services</td>
      <td>77.2</td>
      <td>%</td>
    </tr>
    <tr>
      <th>32</th>
      <td>Denmark</td>
      <td>2017</td>
      <td>Services</td>
      <td>75.8</td>
      <td>%</td>
    </tr>
    <tr>
      <th>33</th>
      <td>Finland</td>
      <td>2017</td>
      <td>Services</td>
      <td>69.1</td>
      <td>%</td>
    </tr>
    <tr>
      <th>34</th>
      <td>France</td>
      <td>2017</td>
      <td>Services</td>
      <td>78.8</td>
      <td>%</td>
    </tr>
    <tr>
      <th>35</th>
      <td>Germany</td>
      <td>2017</td>
      <td>Services</td>
      <td>68.6</td>
      <td>%</td>
    </tr>
    <tr>
      <th>36</th>
      <td>Greece</td>
      <td>2017</td>
      <td>Services</td>
      <td>79.1</td>
      <td>%</td>
    </tr>
    <tr>
      <th>37</th>
      <td>Ireland</td>
      <td>2017</td>
      <td>Services</td>
      <td>60.2</td>
      <td>%</td>
    </tr>
    <tr>
      <th>38</th>
      <td>Italy</td>
      <td>2017</td>
      <td>Services</td>
      <td>73.9</td>
      <td>%</td>
    </tr>
    <tr>
      <th>39</th>
      <td>Luxembourg</td>
      <td>2017</td>
      <td>Services</td>
      <td>86.9</td>
      <td>%</td>
    </tr>
    <tr>
      <th>40</th>
      <td>Netherlands</td>
      <td>2013</td>
      <td>Services</td>
      <td>72.6</td>
      <td>%</td>
    </tr>
    <tr>
      <th>41</th>
      <td>Portugal</td>
      <td>2017</td>
      <td>Services</td>
      <td>75.7</td>
      <td>%</td>
    </tr>
    <tr>
      <th>42</th>
      <td>Spain</td>
      <td>2017</td>
      <td>Services</td>
      <td>74.2</td>
      <td>%</td>
    </tr>
    <tr>
      <th>43</th>
      <td>Sweden</td>
      <td>2017</td>
      <td>Services</td>
      <td>65.4</td>
      <td>%</td>
    </tr>
    <tr>
      <th>44</th>
      <td>United Kindgom</td>
      <td>2017</td>
      <td>Services</td>
      <td>79.2</td>
      <td>%</td>
    </tr>
  </tbody>
</table>
</div>




```python
chartCountries = alt.Chart(dfComposition_EU15).mark_bar().encode(
    x = alt.X('value:Q', scale=alt.Scale(domain=(0,100)), title='Proportion (%)'),
    y = 'Country:N',
    color = 'Sector:N'
)
chartCountries
```





<div id="altair-viz-8cc33edfa52244089873276b0319cb6c"></div>
<script type="text/javascript">
  (function(spec, embedOpt){
    let outputDiv = document.currentScript.previousElementSibling;
    if (outputDiv.id !== "altair-viz-8cc33edfa52244089873276b0319cb6c") {
      outputDiv = document.getElementById("altair-viz-8cc33edfa52244089873276b0319cb6c");
    }
    const paths = {
      "vega": "https://cdn.jsdelivr.net/npm//vega@5?noext",
      "vega-lib": "https://cdn.jsdelivr.net/npm//vega-lib?noext",
      "vega-lite": "https://cdn.jsdelivr.net/npm//vega-lite@4.8.1?noext",
      "vega-embed": "https://cdn.jsdelivr.net/npm//vega-embed@6?noext",
    };

    function loadScript(lib) {
      return new Promise(function(resolve, reject) {
        var s = document.createElement('script');
        s.src = paths[lib];
        s.async = true;
        s.onload = () => resolve(paths[lib]);
        s.onerror = () => reject(`Error loading script: ${paths[lib]}`);
        document.getElementsByTagName("head")[0].appendChild(s);
      });
    }

    function showError(err) {
      outputDiv.innerHTML = `<div class="error" style="color:red;">${err}</div>`;
      throw err;
    }

    function displayChart(vegaEmbed) {
      vegaEmbed(outputDiv, spec, embedOpt)
        .catch(err => showError(`Javascript Error: ${err.message}<br>This usually means there's a typo in your chart specification. See the javascript console for the full traceback.`));
    }

    if(typeof define === "function" && define.amd) {
      requirejs.config({paths});
      require(["vega-embed"], displayChart, err => showError(`Error loading script: ${err.message}`));
    } else if (typeof vegaEmbed === "function") {
      displayChart(vegaEmbed);
    } else {
      loadScript("vega")
        .then(() => loadScript("vega-lite"))
        .then(() => loadScript("vega-embed"))
        .catch(showError)
        .then(() => displayChart(vegaEmbed));
    }
  })({"config": {"view": {"continuousWidth": 400, "continuousHeight": 300}}, "data": {"name": "data-0c1694934a16ad17c0269388b95b4727"}, "mark": "bar", "encoding": {"color": {"type": "nominal", "field": "Sector"}, "x": {"type": "quantitative", "field": "value", "scale": {"domain": [0, 100]}, "title": "Value (%)"}, "y": {"type": "nominal", "field": "Country"}}, "$schema": "https://vega.github.io/schema/vega-lite/v4.8.1.json", "datasets": {"data-0c1694934a16ad17c0269388b95b4727": [{"Country": "Austria", "Year": "2017", "Sector": "Agriculture", "value": 1.3, "Units": "%"}, {"Country": "Belgium", "Year": "2017", "Sector": "Agriculture", "value": 0.7000000000000001, "Units": "%"}, {"Country": "Denmark", "Year": "2017", "Sector": "Agriculture", "value": 1.3, "Units": "%"}, {"Country": "Finland", "Year": "2017", "Sector": "Agriculture", "value": 2.7, "Units": "%"}, {"Country": "France", "Year": "2017", "Sector": "Agriculture", "value": 1.7000000000000002, "Units": "%"}, {"Country": "Germany", "Year": "2017", "Sector": "Agriculture", "value": 0.7000000000000001, "Units": "%"}, {"Country": "Greece", "Year": "2017", "Sector": "Agriculture", "value": 4.1, "Units": "%"}, {"Country": "Ireland", "Year": "2017", "Sector": "Agriculture", "value": 1.2, "Units": "%"}, {"Country": "Italy", "Year": "2017", "Sector": "Agriculture", "value": 2.1, "Units": "%"}, {"Country": "Luxembourg", "Year": "2017", "Sector": "Agriculture", "value": 0.30000000000000004, "Units": "%"}, {"Country": "Netherlands", "Year": 2013, "Sector": "Agriculture", "value": 2.5, "Units": "%"}, {"Country": "Portugal", "Year": "2017", "Sector": "Agriculture", "value": 2.2, "Units": "%"}, {"Country": "Spain", "Year": "2017", "Sector": "Agriculture", "value": 2.6, "Units": "%"}, {"Country": "Sweden", "Year": "2017", "Sector": "Agriculture", "value": 1.6, "Units": "%"}, {"Country": "United Kindgom", "Year": "2017", "Sector": "Agriculture", "value": 0.7000000000000001, "Units": "%"}, {"Country": "Austria", "Year": "2017", "Sector": "Industry", "value": 28.4, "Units": "%"}, {"Country": "Belgium", "Year": "2017", "Sector": "Industry", "value": 22.1, "Units": "%"}, {"Country": "Denmark", "Year": "2017", "Sector": "Industry", "value": 22.9, "Units": "%"}, {"Country": "Finland", "Year": "2017", "Sector": "Industry", "value": 28.2, "Units": "%"}, {"Country": "France", "Year": "2017", "Sector": "Industry", "value": 19.5, "Units": "%"}, {"Country": "Germany", "Year": "2017", "Sector": "Industry", "value": 30.7, "Units": "%"}, {"Country": "Greece", "Year": "2017", "Sector": "Industry", "value": 16.9, "Units": "%"}, {"Country": "Ireland", "Year": "2017", "Sector": "Industry", "value": 38.6, "Units": "%"}, {"Country": "Italy", "Year": "2017", "Sector": "Industry", "value": 23.9, "Units": "%"}, {"Country": "Luxembourg", "Year": "2017", "Sector": "Industry", "value": 12.8, "Units": "%"}, {"Country": "Netherlands", "Year": 2013, "Sector": "Industry", "value": 24.9, "Units": "%"}, {"Country": "Portugal", "Year": "2017", "Sector": "Industry", "value": 22.1, "Units": "%"}, {"Country": "Spain", "Year": "2017", "Sector": "Industry", "value": 23.2, "Units": "%"}, {"Country": "Sweden", "Year": "2017", "Sector": "Industry", "value": 33.0, "Units": "%"}, {"Country": "United Kindgom", "Year": "2017", "Sector": "Industry", "value": 20.2, "Units": "%"}, {"Country": "Austria", "Year": "2017", "Sector": "Services", "value": 70.3, "Units": "%"}, {"Country": "Belgium", "Year": "2017", "Sector": "Services", "value": 77.2, "Units": "%"}, {"Country": "Denmark", "Year": "2017", "Sector": "Services", "value": 75.8, "Units": "%"}, {"Country": "Finland", "Year": "2017", "Sector": "Services", "value": 69.1, "Units": "%"}, {"Country": "France", "Year": "2017", "Sector": "Services", "value": 78.8, "Units": "%"}, {"Country": "Germany", "Year": "2017", "Sector": "Services", "value": 68.6, "Units": "%"}, {"Country": "Greece", "Year": "2017", "Sector": "Services", "value": 79.1, "Units": "%"}, {"Country": "Ireland", "Year": "2017", "Sector": "Services", "value": 60.2, "Units": "%"}, {"Country": "Italy", "Year": "2017", "Sector": "Services", "value": 73.9, "Units": "%"}, {"Country": "Luxembourg", "Year": "2017", "Sector": "Services", "value": 86.9, "Units": "%"}, {"Country": "Netherlands", "Year": 2013, "Sector": "Services", "value": 72.6, "Units": "%"}, {"Country": "Portugal", "Year": "2017", "Sector": "Services", "value": 75.7, "Units": "%"}, {"Country": "Spain", "Year": "2017", "Sector": "Services", "value": 74.2, "Units": "%"}, {"Country": "Sweden", "Year": "2017", "Sector": "Services", "value": 65.4, "Units": "%"}, {"Country": "United Kindgom", "Year": "2017", "Sector": "Services", "value": 79.2, "Units": "%"}]}}, {"mode": "vega-lite"});
</script>




```python
# Create a list to sort the next part in
sortedServices = dfComposition_EU15[dfComposition_EU15['Sector'] == 'Services'] \
                    .set_index('Country')['value'].sort_values(ascending=False).index.to_list()
```


```python
chartServices = alt.Chart(dfComposition_EU15).mark_bar().encode(
    x = alt.X('value:Q', scale=alt.Scale(domain=(0,100)), title='Proportion (%)'),
    y = alt.Y('Country:N', sort=sortedServices),
    color ='Sector:N',
    order=alt.Order('Sector:N', sort='descending')
)
chartServices

```





<div id="altair-viz-6e2d4aaa729f4381b0fce445e234e90a"></div>
<script type="text/javascript">
  (function(spec, embedOpt){
    let outputDiv = document.currentScript.previousElementSibling;
    if (outputDiv.id !== "altair-viz-6e2d4aaa729f4381b0fce445e234e90a") {
      outputDiv = document.getElementById("altair-viz-6e2d4aaa729f4381b0fce445e234e90a");
    }
    const paths = {
      "vega": "https://cdn.jsdelivr.net/npm//vega@5?noext",
      "vega-lib": "https://cdn.jsdelivr.net/npm//vega-lib?noext",
      "vega-lite": "https://cdn.jsdelivr.net/npm//vega-lite@4.8.1?noext",
      "vega-embed": "https://cdn.jsdelivr.net/npm//vega-embed@6?noext",
    };

    function loadScript(lib) {
      return new Promise(function(resolve, reject) {
        var s = document.createElement('script');
        s.src = paths[lib];
        s.async = true;
        s.onload = () => resolve(paths[lib]);
        s.onerror = () => reject(`Error loading script: ${paths[lib]}`);
        document.getElementsByTagName("head")[0].appendChild(s);
      });
    }

    function showError(err) {
      outputDiv.innerHTML = `<div class="error" style="color:red;">${err}</div>`;
      throw err;
    }

    function displayChart(vegaEmbed) {
      vegaEmbed(outputDiv, spec, embedOpt)
        .catch(err => showError(`Javascript Error: ${err.message}<br>This usually means there's a typo in your chart specification. See the javascript console for the full traceback.`));
    }

    if(typeof define === "function" && define.amd) {
      requirejs.config({paths});
      require(["vega-embed"], displayChart, err => showError(`Error loading script: ${err.message}`));
    } else if (typeof vegaEmbed === "function") {
      displayChart(vegaEmbed);
    } else {
      loadScript("vega")
        .then(() => loadScript("vega-lite"))
        .then(() => loadScript("vega-embed"))
        .catch(showError)
        .then(() => displayChart(vegaEmbed));
    }
  })({"config": {"view": {"continuousWidth": 400, "continuousHeight": 300}}, "data": {"name": "data-0c1694934a16ad17c0269388b95b4727"}, "mark": "bar", "encoding": {"color": {"type": "nominal", "field": "Sector"}, "x": {"type": "quantitative", "field": "value", "scale": {"domain": [0, 100]}, "title": "Value (%)"}, "y": {"type": "nominal", "field": "Country", "sort": ["Luxembourg", "United Kindgom", "Greece", "France", "Belgium", "Denmark", "Portugal", "Spain", "Italy", "Netherlands", "Austria", "Finland", "Germany", "Sweden", "Ireland"]}}, "$schema": "https://vega.github.io/schema/vega-lite/v4.8.1.json", "datasets": {"data-0c1694934a16ad17c0269388b95b4727": [{"Country": "Austria", "Year": "2017", "Sector": "Agriculture", "value": 1.3, "Units": "%"}, {"Country": "Belgium", "Year": "2017", "Sector": "Agriculture", "value": 0.7000000000000001, "Units": "%"}, {"Country": "Denmark", "Year": "2017", "Sector": "Agriculture", "value": 1.3, "Units": "%"}, {"Country": "Finland", "Year": "2017", "Sector": "Agriculture", "value": 2.7, "Units": "%"}, {"Country": "France", "Year": "2017", "Sector": "Agriculture", "value": 1.7000000000000002, "Units": "%"}, {"Country": "Germany", "Year": "2017", "Sector": "Agriculture", "value": 0.7000000000000001, "Units": "%"}, {"Country": "Greece", "Year": "2017", "Sector": "Agriculture", "value": 4.1, "Units": "%"}, {"Country": "Ireland", "Year": "2017", "Sector": "Agriculture", "value": 1.2, "Units": "%"}, {"Country": "Italy", "Year": "2017", "Sector": "Agriculture", "value": 2.1, "Units": "%"}, {"Country": "Luxembourg", "Year": "2017", "Sector": "Agriculture", "value": 0.30000000000000004, "Units": "%"}, {"Country": "Netherlands", "Year": 2013, "Sector": "Agriculture", "value": 2.5, "Units": "%"}, {"Country": "Portugal", "Year": "2017", "Sector": "Agriculture", "value": 2.2, "Units": "%"}, {"Country": "Spain", "Year": "2017", "Sector": "Agriculture", "value": 2.6, "Units": "%"}, {"Country": "Sweden", "Year": "2017", "Sector": "Agriculture", "value": 1.6, "Units": "%"}, {"Country": "United Kindgom", "Year": "2017", "Sector": "Agriculture", "value": 0.7000000000000001, "Units": "%"}, {"Country": "Austria", "Year": "2017", "Sector": "Industry", "value": 28.4, "Units": "%"}, {"Country": "Belgium", "Year": "2017", "Sector": "Industry", "value": 22.1, "Units": "%"}, {"Country": "Denmark", "Year": "2017", "Sector": "Industry", "value": 22.9, "Units": "%"}, {"Country": "Finland", "Year": "2017", "Sector": "Industry", "value": 28.2, "Units": "%"}, {"Country": "France", "Year": "2017", "Sector": "Industry", "value": 19.5, "Units": "%"}, {"Country": "Germany", "Year": "2017", "Sector": "Industry", "value": 30.7, "Units": "%"}, {"Country": "Greece", "Year": "2017", "Sector": "Industry", "value": 16.9, "Units": "%"}, {"Country": "Ireland", "Year": "2017", "Sector": "Industry", "value": 38.6, "Units": "%"}, {"Country": "Italy", "Year": "2017", "Sector": "Industry", "value": 23.9, "Units": "%"}, {"Country": "Luxembourg", "Year": "2017", "Sector": "Industry", "value": 12.8, "Units": "%"}, {"Country": "Netherlands", "Year": 2013, "Sector": "Industry", "value": 24.9, "Units": "%"}, {"Country": "Portugal", "Year": "2017", "Sector": "Industry", "value": 22.1, "Units": "%"}, {"Country": "Spain", "Year": "2017", "Sector": "Industry", "value": 23.2, "Units": "%"}, {"Country": "Sweden", "Year": "2017", "Sector": "Industry", "value": 33.0, "Units": "%"}, {"Country": "United Kindgom", "Year": "2017", "Sector": "Industry", "value": 20.2, "Units": "%"}, {"Country": "Austria", "Year": "2017", "Sector": "Services", "value": 70.3, "Units": "%"}, {"Country": "Belgium", "Year": "2017", "Sector": "Services", "value": 77.2, "Units": "%"}, {"Country": "Denmark", "Year": "2017", "Sector": "Services", "value": 75.8, "Units": "%"}, {"Country": "Finland", "Year": "2017", "Sector": "Services", "value": 69.1, "Units": "%"}, {"Country": "France", "Year": "2017", "Sector": "Services", "value": 78.8, "Units": "%"}, {"Country": "Germany", "Year": "2017", "Sector": "Services", "value": 68.6, "Units": "%"}, {"Country": "Greece", "Year": "2017", "Sector": "Services", "value": 79.1, "Units": "%"}, {"Country": "Ireland", "Year": "2017", "Sector": "Services", "value": 60.2, "Units": "%"}, {"Country": "Italy", "Year": "2017", "Sector": "Services", "value": 73.9, "Units": "%"}, {"Country": "Luxembourg", "Year": "2017", "Sector": "Services", "value": 86.9, "Units": "%"}, {"Country": "Netherlands", "Year": 2013, "Sector": "Services", "value": 72.6, "Units": "%"}, {"Country": "Portugal", "Year": "2017", "Sector": "Services", "value": 75.7, "Units": "%"}, {"Country": "Spain", "Year": "2017", "Sector": "Services", "value": 74.2, "Units": "%"}, {"Country": "Sweden", "Year": "2017", "Sector": "Services", "value": 65.4, "Units": "%"}, {"Country": "United Kindgom", "Year": "2017", "Sector": "Services", "value": 79.2, "Units": "%"}]}}, {"mode": "vega-lite"});
</script>




```python
chartCountries | chartServices 
```





<div id="altair-viz-c81f85fd0ac047c29b71d6a73c93c4f0"></div>
<script type="text/javascript">
  (function(spec, embedOpt){
    let outputDiv = document.currentScript.previousElementSibling;
    if (outputDiv.id !== "altair-viz-c81f85fd0ac047c29b71d6a73c93c4f0") {
      outputDiv = document.getElementById("altair-viz-c81f85fd0ac047c29b71d6a73c93c4f0");
    }
    const paths = {
      "vega": "https://cdn.jsdelivr.net/npm//vega@5?noext",
      "vega-lib": "https://cdn.jsdelivr.net/npm//vega-lib?noext",
      "vega-lite": "https://cdn.jsdelivr.net/npm//vega-lite@4.8.1?noext",
      "vega-embed": "https://cdn.jsdelivr.net/npm//vega-embed@6?noext",
    };

    function loadScript(lib) {
      return new Promise(function(resolve, reject) {
        var s = document.createElement('script');
        s.src = paths[lib];
        s.async = true;
        s.onload = () => resolve(paths[lib]);
        s.onerror = () => reject(`Error loading script: ${paths[lib]}`);
        document.getElementsByTagName("head")[0].appendChild(s);
      });
    }

    function showError(err) {
      outputDiv.innerHTML = `<div class="error" style="color:red;">${err}</div>`;
      throw err;
    }

    function displayChart(vegaEmbed) {
      vegaEmbed(outputDiv, spec, embedOpt)
        .catch(err => showError(`Javascript Error: ${err.message}<br>This usually means there's a typo in your chart specification. See the javascript console for the full traceback.`));
    }

    if(typeof define === "function" && define.amd) {
      requirejs.config({paths});
      require(["vega-embed"], displayChart, err => showError(`Error loading script: ${err.message}`));
    } else if (typeof vegaEmbed === "function") {
      displayChart(vegaEmbed);
    } else {
      loadScript("vega")
        .then(() => loadScript("vega-lite"))
        .then(() => loadScript("vega-embed"))
        .catch(showError)
        .then(() => displayChart(vegaEmbed));
    }
  })({"config": {"view": {"continuousWidth": 400, "continuousHeight": 300}}, "hconcat": [{"mark": "bar", "encoding": {"color": {"type": "nominal", "field": "Sector"}, "x": {"type": "quantitative", "field": "value", "scale": {"domain": [0, 100]}, "title": "Value (%)"}, "y": {"type": "nominal", "field": "Country"}}}, {"mark": "bar", "encoding": {"color": {"type": "nominal", "field": "Sector"}, "x": {"type": "quantitative", "field": "value", "scale": {"domain": [0, 100]}, "title": "Value (%)"}, "y": {"type": "nominal", "field": "Country", "sort": ["Luxembourg", "United Kindgom", "Greece", "France", "Belgium", "Denmark", "Portugal", "Spain", "Italy", "Netherlands", "Austria", "Finland", "Germany", "Sweden", "Ireland"]}}}], "data": {"name": "data-0c1694934a16ad17c0269388b95b4727"}, "$schema": "https://vega.github.io/schema/vega-lite/v4.8.1.json", "datasets": {"data-0c1694934a16ad17c0269388b95b4727": [{"Country": "Austria", "Year": "2017", "Sector": "Agriculture", "value": 1.3, "Units": "%"}, {"Country": "Belgium", "Year": "2017", "Sector": "Agriculture", "value": 0.7000000000000001, "Units": "%"}, {"Country": "Denmark", "Year": "2017", "Sector": "Agriculture", "value": 1.3, "Units": "%"}, {"Country": "Finland", "Year": "2017", "Sector": "Agriculture", "value": 2.7, "Units": "%"}, {"Country": "France", "Year": "2017", "Sector": "Agriculture", "value": 1.7000000000000002, "Units": "%"}, {"Country": "Germany", "Year": "2017", "Sector": "Agriculture", "value": 0.7000000000000001, "Units": "%"}, {"Country": "Greece", "Year": "2017", "Sector": "Agriculture", "value": 4.1, "Units": "%"}, {"Country": "Ireland", "Year": "2017", "Sector": "Agriculture", "value": 1.2, "Units": "%"}, {"Country": "Italy", "Year": "2017", "Sector": "Agriculture", "value": 2.1, "Units": "%"}, {"Country": "Luxembourg", "Year": "2017", "Sector": "Agriculture", "value": 0.30000000000000004, "Units": "%"}, {"Country": "Netherlands", "Year": 2013, "Sector": "Agriculture", "value": 2.5, "Units": "%"}, {"Country": "Portugal", "Year": "2017", "Sector": "Agriculture", "value": 2.2, "Units": "%"}, {"Country": "Spain", "Year": "2017", "Sector": "Agriculture", "value": 2.6, "Units": "%"}, {"Country": "Sweden", "Year": "2017", "Sector": "Agriculture", "value": 1.6, "Units": "%"}, {"Country": "United Kindgom", "Year": "2017", "Sector": "Agriculture", "value": 0.7000000000000001, "Units": "%"}, {"Country": "Austria", "Year": "2017", "Sector": "Industry", "value": 28.4, "Units": "%"}, {"Country": "Belgium", "Year": "2017", "Sector": "Industry", "value": 22.1, "Units": "%"}, {"Country": "Denmark", "Year": "2017", "Sector": "Industry", "value": 22.9, "Units": "%"}, {"Country": "Finland", "Year": "2017", "Sector": "Industry", "value": 28.2, "Units": "%"}, {"Country": "France", "Year": "2017", "Sector": "Industry", "value": 19.5, "Units": "%"}, {"Country": "Germany", "Year": "2017", "Sector": "Industry", "value": 30.7, "Units": "%"}, {"Country": "Greece", "Year": "2017", "Sector": "Industry", "value": 16.9, "Units": "%"}, {"Country": "Ireland", "Year": "2017", "Sector": "Industry", "value": 38.6, "Units": "%"}, {"Country": "Italy", "Year": "2017", "Sector": "Industry", "value": 23.9, "Units": "%"}, {"Country": "Luxembourg", "Year": "2017", "Sector": "Industry", "value": 12.8, "Units": "%"}, {"Country": "Netherlands", "Year": 2013, "Sector": "Industry", "value": 24.9, "Units": "%"}, {"Country": "Portugal", "Year": "2017", "Sector": "Industry", "value": 22.1, "Units": "%"}, {"Country": "Spain", "Year": "2017", "Sector": "Industry", "value": 23.2, "Units": "%"}, {"Country": "Sweden", "Year": "2017", "Sector": "Industry", "value": 33.0, "Units": "%"}, {"Country": "United Kindgom", "Year": "2017", "Sector": "Industry", "value": 20.2, "Units": "%"}, {"Country": "Austria", "Year": "2017", "Sector": "Services", "value": 70.3, "Units": "%"}, {"Country": "Belgium", "Year": "2017", "Sector": "Services", "value": 77.2, "Units": "%"}, {"Country": "Denmark", "Year": "2017", "Sector": "Services", "value": 75.8, "Units": "%"}, {"Country": "Finland", "Year": "2017", "Sector": "Services", "value": 69.1, "Units": "%"}, {"Country": "France", "Year": "2017", "Sector": "Services", "value": 78.8, "Units": "%"}, {"Country": "Germany", "Year": "2017", "Sector": "Services", "value": 68.6, "Units": "%"}, {"Country": "Greece", "Year": "2017", "Sector": "Services", "value": 79.1, "Units": "%"}, {"Country": "Ireland", "Year": "2017", "Sector": "Services", "value": 60.2, "Units": "%"}, {"Country": "Italy", "Year": "2017", "Sector": "Services", "value": 73.9, "Units": "%"}, {"Country": "Luxembourg", "Year": "2017", "Sector": "Services", "value": 86.9, "Units": "%"}, {"Country": "Netherlands", "Year": 2013, "Sector": "Services", "value": 72.6, "Units": "%"}, {"Country": "Portugal", "Year": "2017", "Sector": "Services", "value": 75.7, "Units": "%"}, {"Country": "Spain", "Year": "2017", "Sector": "Services", "value": 74.2, "Units": "%"}, {"Country": "Sweden", "Year": "2017", "Sector": "Services", "value": 65.4, "Units": "%"}, {"Country": "United Kindgom", "Year": "2017", "Sector": "Services", "value": 79.2, "Units": "%"}]}}, {"mode": "vega-lite"});
</script>




```python
chartServices.save('../reports/figures/GDPCompositionServices.pdf')
```


## GDP and Population Forecasts

```python
dfIIASA_GDP = pd.read_csv('../data/external/iiasa_gdp.csv',index_col=0)
dfIIASA_Population = pd.read_csv('../data/external/iiasa_population.csv',index_col=0)
```

```python
countryMapIIASA = {'BEL':'Belgium','GRC':'Greece','PRT':'Portugal','ESP':'Spain','LUX':'Luxembourg','FRA':'France','DNK':'Denmark','DEU':'Germany','ITA':'Italy','NLD':'Netherlands','FIN':'Finland','AUT':'Austria','SWE':'Sweden','IRL':'Ireland','GBR':'United Kingdom'}
countryMapIIASA = {code.lower(): country for code, country in countryMapIIASA.items()}

dfIIASA_GDP = dfIIASA_GDP.rename(columns={'iso':'geo'}).query("geo.isin(@countryMapIIASA.keys())").replace(countryMapIIASA)
dfIIASA_Population = dfIIASA_Population.rename(columns={'iso':'geo'}).query("geo.isin(@countryMapIIASA.keys())").replace(countryMapIIASA)

# Align to Eurostat country codes
countryMap = {'BE':'Belgium','EL':'Greece','PT':'Portugal','ES':'Spain','LU':'Luxembourg','FR':'France','DK':'Denmark','DE':'Germany','IT':'Italy','NL':'Netherlands','FI':'Finland','AT':'Austria','SE':'Sweden','IE':'Ireland','UK':'United Kingdom'}
countryMapInv = {v: k for k, v in countryMap.items()}

dfIIASA_GDP = dfIIASA_GDP.replace(countryMapInv)
dfIIASA_Population = dfIIASA_Population.replace(countryMapInv)
dfIIASA_GDP.head()
```

```python
dfIIASA_GDP = dfIIASA_GDP.reset_index().drop(columns='model').melt(id_vars=['geo','scenario','variable','unit'], var_name='Year')
dfIIASA_GDP['Year'] = dfIIASA_GDP['Year'].str.lstrip('X').astype(int)
dfIIASA_GDP['Year'] = pd.to_datetime(dfIIASA_GDP['Year'], format='%Y')
```

```python
dfIIASA_Population = dfIIASA_Population.reset_index().drop(columns='model').melt(id_vars=['geo','scenario','variable','unit'], var_name='Year')
dfIIASA_Population['Year'] = dfIIASA_Population['Year'].str.lstrip('X').astype(int)
dfIIASA_Population['Year'] = pd.to_datetime(dfIIASA_Population['Year'], format='%Y')
```

```python
dfIIASA_GDPPopulation = dfIIASA_GDP.append(dfIIASA_Population)
dfIIASA_GDPPopulation
```

Calculate the propotion relative to 2018, the base year.

```python
dfIIASA_GDPPopulation = (dfIIASA_GDPPopulation.set_index(['scenario','geo','variable','Year'])['value'] / dfIIASA_GDPPopulation.query("Year.dt.year==2018").set_index(['scenario','geo','variable'])['value']).reset_index()
dfIIASA_GDPPopulation
```

```python
alt.data_transformers.disable_max_rows()
chart = alt.Chart(dfIIASA_GDPPopulation).mark_line().encode(
    x = 'Year:T',
    y = alt.Y('value:Q', title='Value (vs. 2018)'),
    color = alt.Color('variable:N', title='Variable')
).properties(
    height=100,
    width=100
).transform_filter(
    alt.datum.scenario=='SSP2'
).facet(
    facet=alt.Facet('geo:N',title='Country'),
    columns=4
)
chart
```

```python
chart.save('../reports/figures/iiasaGDPPopulation.pdf')
```

```python

```
