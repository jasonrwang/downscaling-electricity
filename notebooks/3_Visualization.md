---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.3.2
  kernelspec:
    display_name: 'Python 3.8.1 64-bit (''3.8.1'': pyenv)'
    language: python
    name: python38164bit381pyenvb3df2019228041fdaa1b32128074601a
---

# 3 Visualization


Visualizing the differences between the different methods of downscaling.

```python
import pandas as pd
import altair as alt
# from plotnine import *
```

Define a theme that deals with more than 10 categorical values (i.e. we have 15 countries and the default behaviour only works with the 10 here https://vega.github.io/vega/docs/schemes/#categorical).


Import Source Sans Pro font from Google Fonts. (invisible below)

```html
<style>
@import url('https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap');
</style>
```

```python
def my_theme():
    return {
        'config': {'range': {'category': {'scheme': 'category20'}},
                    'font': 'Source Sans Pro'}
    }

alt.themes.register('my_theme', my_theme)
alt.themes.enable('my_theme')
```

```python
countryOfInterest = 'NL'
baseYear = 2018
```

<!-- #region toc-hr-collapsed=true toc-nb-collapsed=true -->
## GCAM Regional Trend
<!-- #endregion -->

```python
# Some of these columns are needed for the other comparisons
dfGCAM = pd.read_csv('../data/processed/GCAM_energyRecategorized.csv', index_col=0, parse_dates=['Year'])
dfGCAM['type'] = 'GCAM Recategorized'
dfGCAM['downscaleType'] = 'None'
dfGCAM.head()
```

```python
# Do the same but for the linear data
dfGCAM_Original = pd.read_csv('../data/processed/GCAM_energyOriginal.csv', index_col=0, parse_dates=['Year'])
dfGCAM_Original['type'] = 'GCAM Original'
dfGCAM_Original['downscaleType'] = 'None'
dfGCAM_Original.head()
```

<!-- #region toc-hr-collapsed=true toc-nb-collapsed=true -->
### Current Energy Use

Import the current use so we can compare with the model's values.
<!-- #endregion -->

```python
dfCurrent = pd.read_csv('../data/interim/eurostatEnergy.csv', index_col=0, parse_dates=['year'])
```

```python
dfCurrent = dfCurrent.query("year==@baseYear")
dfCurrent['scenario'] = 'Base ({:.0f})'.format(baseYear)
dfCurrent['type'] = 'baseYear'
dfCurrent['downscaleType'] = 'Base'
dfCurrent['year'] = pd.to_datetime(dfCurrent['year'],format='%Y')
dfCurrent = dfCurrent.rename(columns={'unit':'Units','geo':'region','siecName':'technology','year':'Year'})
dfCurrent.head()
```

```python
dfCurrentBase = dfCurrent.groupby(['technology','Year','scenario'], as_index=False).agg({'value':'sum','Units':'first','type':'first','downscaleType':'first'})
dfCurrentBase['region'] = 'EU-15'
dfCurrentBase
```

```python
dfGCAM = dfGCAM.append(dfCurrentBase)
```

```python
dfGCAM
```

```python
series = alt.Chart().mark_line().encode(
    alt.X('Year:T', title='Year'),
    alt.Y('value:Q', axis=alt.Axis(title='Energy (EJ)', format=',.1f')), # format='%', 
    color=alt.Color('type:N', title=['Data Source'])
).properties(
    width=200,
    height=70
).transform_filter(
    (alt.datum.type != 'baseYear')
)

line = alt.Chart().mark_rule(color='darkorange',strokeDash=[4,4]).encode(
    y='value:Q',
    size=alt.SizeValue(1),
    color='type:N'
).transform_filter(
    (alt.datum.type == 'baseYear')
)

rule = alt.Chart().mark_rule(color='darkorange',strokeDash=[2,2],opacity=0.6).encode(
    x='Year:T',
    size=alt.SizeValue(1)
).transform_filter(
    (alt.datum.type == 'baseYear')
)

# Taken from example https://github.com/altair-viz/altair/issues/920
chart = alt.layer(series, line, rule, data=dfGCAM).facet(
    facet=alt.Facet('technology:N', title='Resource Type'),
    columns=3
).resolve_scale(y='independent')

chart
# Data types are declared with the colon syntax. See more at https://altair-viz.github.io/user_guide/encoding.html#encoding-data-types
```

```python
chart.save('../reports/figures/GCAMvsActualBase_Recategorized.pdf')
```

```python
chart = alt.layer(series, line, rule, data=dfGCAM_Original).facet(
    facet=alt.Facet('technology:N', title='Resource Type'),
    columns=3
).resolve_scale(y='independent')

chart
```

```python
chart.save('../reports/figures/GCAMvsActualBase_Original.pdf')
```

## Linear Downscaling


### Original Energy Categories

```python
dfData_linearPopulation = pd.read_csv('../data/processed/linearPopulation_Original.csv', index_col=0, parse_dates=['Year'])
dfData_linearGDP = pd.read_csv('../data/processed/linearGDP_Original.csv', index_col=0, parse_dates=['Year'])
dfData_linearEmissions = pd.read_csv('../data/processed/linearEmissions_Original.csv', index_col=0, parse_dates=['Year'])
```

```python
dfData_linearPopulation.head()
```

```python
dfData_linearGDP.head()
```

```python
dfData_linearEmissions.head()
```

```python
dfData_linearPopulation['type'] = 'linearPopulation'
dfData_linearGDP['type'] = 'linearGDP'
dfData_linearEmissions['type'] = 'linearEmissions'
```

```python
dfLinear = pd.DataFrame()
dfLinear = dfLinear.append([dfData_linearPopulation, dfData_linearGDP, dfData_linearEmissions])
dfLinear['downscaleType'] = 'Linear'
```

```python
dfLinear['technology'].unique()
```

```python
dfLinear
```

```python
dfLinear['Year'] = pd.to_datetime(dfLinear['Year'],format='%Y')
```

```python
# dfLinear = dfLinear.append(dfCurrent)
```

```python
alt.data_transformers.disable_max_rows()

chart = alt.Chart().mark_line().encode(
    alt.X('Year:T'),
    alt.Y('value:Q', axis=alt.Axis(title='Energy (EJ)')), # format='%', 
    color=alt.Color('type:N', title=['Downscaling','Relative Parameter'])
).properties(
    width=200,
    height=70
).transform_filter(
    (alt.datum.type != 'baseYear')
)

rule = alt.Chart().mark_rule(strokeDash=[3,3]).encode(
    y='value:Q',
    size=alt.SizeValue(1),
    color='type:N'
).transform_filter(
    (alt.datum.type == 'baseYear')
)

# Taken from example https://github.com/altair-viz/altair/issues/920
chart = alt.layer(chart, rule, data=dfLinear).facet(
    facet=alt.Facet('technology:N', title='Resource Type'),
    columns=3
).transform_filter(
    (alt.datum['region'] == countryOfInterest)
).resolve_scale(y='independent')

chart
# Data types are declared with the colon syntax. See more at https://altair-viz.github.io/user_guide/encoding.html#encoding-data-types
```

```python
chart.save('../reports/figures/linearDownscaled_Original.pdf')
```

Normalize scales (easy in Altair)
Maybe categorize by Resource Type


#### Compilation of Proportions


We can compare how the downscaling worked across the different methods.

```python
pd.read_csv('../data/processed/linearProportionsEmissions_Original.csv', index_col=0, parse_dates=['Year'])
```

```python
dfEmissionsProportion = pd.read_csv('../data/processed/linearProportionsEmissions_Original.csv', index_col=0, parse_dates=['Year'])
dfGDPProportion = pd.read_csv('../data/processed/linearProportionsGDP_Original.csv', index_col=0, parse_dates=['Year'])
dfPopulationProportion = pd.read_csv('../data/processed/linearProportionsPopulation_Original.csv', index_col=0, parse_dates=['Year'])
```

```python
# Join all the separate proportion DataFrames together
dfLinearProportions = dfEmissionsProportion.query("Year==2018").set_index('geo')['value'].to_frame().rename(columns={'value':'Emissions'})
dfLinearProportions = dfLinearProportions.join(dfPopulationProportion.query("Year==2018").set_index('geo').rename(columns={'value':'Population'})['Population'])
dfLinearProportions = dfLinearProportions.join(dfGDPProportion.query("Year==2018").set_index('geo').rename(columns={'value':'GDP'})['GDP'])
dfLinearProportions = dfLinearProportions.reset_index().rename(columns={'geo':'Country'}).set_index('Country')

# Change the reported value to percents
dfLinearProportions = dfLinearProportions.apply(lambda x: x*100)

dfLinearProportions.to_latex('../data/processed/linearProportions_Original.tex', float_format="{:0.1f}".format)
```

```python
dfLinearProportions = dfLinearProportions.reset_index().melt(id_vars='Country')
```

```python
chart = alt.Chart().mark_bar().encode(
    x=alt.X('Country:N', title='Country'),
    y=alt.Y('value:Q', title="Proportion (%)"),
    color=alt.Color('Country:N', legend=None)
).properties(
    height=100,
    width=400
)

text = chart.mark_text(
    align='center',
    baseline='middle',
    dy=-10  # Vertical offset
).encode(
    text=alt.Text('value:Q', format=',.0f')
)

# Taken from example https://github.com/altair-viz/altair/issues/920
chart = alt.layer(chart, text, data=dfLinearProportions).facet(
    alt.Facet('variable:N', title='Proportional to'), columns=1
)

chart
```

```python
chart.save('../reports/figures/linearProportions.pdf')
```

Between each variable that emissions are downscaled against, each country performs differently. For some countries, the relative difference is small but the absolute difference is large.

```python
chart = alt.Chart(dfLinearProportions).mark_bar().encode(
    x=alt.X('variable:N', title=None), # Need overall x label 'Downscaled Proportional to'
    y=alt.Y('value:Q', title=None), # Need overall y label ... 'Proportion (%)'
    color=alt.Color('Country:N', legend=None),
    column=alt.Column('Country:N', title='Country'),
).properties(
    height=100,
    width=40
).resolve_scale(y='independent') # Make this not independent and normalize!
chart
```

```python
chart.save('../reports/figures/linearProportionsByCountry.pdf')
```

The absolute differences, in terms of standard deviation, are shown below.

```python
chart = alt.Chart(dfLinearProportions.groupby('Country').std().reset_index()).mark_bar().encode(
    x='Country:N',
    y=alt.Y('value:Q', title='Standard Deviation'),
    color=alt.Color('Country:N', legend=None)
).properties(
    height=200,
    width=400
)
chart
```

```python
chart.save('../reports/figures/linearProportionsCountrySTD.pdf')
```

These values are absolute, but we can also examine the relative difference within each group.

```python
chart = alt.Chart(
    (dfLinearProportions.groupby('Country').std() / dfLinearProportions.groupby('Country').mean()).reset_index()
).mark_bar().encode(
    x='Country:N',
    y=alt.Y('value:Q', title='Coefficient of Variation'),
    color=alt.Color('Country:N', legend=None)
).properties(
    height=200,
    width=400
)
chart
```

```python
chart.save('../reports/figures/linearProportionsCountryCoV.pdf')
```

### Recategorized Energy

```python
dfData_linearPopulation = pd.read_csv('../data/processed/linearPopulation_Recategorized.csv', index_col=0, parse_dates=['Year'])
dfData_linearGDP = pd.read_csv('../data/processed/linearGDP_Recategorized.csv', index_col=0, parse_dates=['Year'])
dfData_linearEmissions = pd.read_csv('../data/processed/linearEmissions_Recategorized.csv', index_col=0, parse_dates=['Year'])
```

```python
dfData_linearPopulation.head()
```

```python
dfData_linearGDP.head()
```

```python
dfData_linearEmissions.head()
```

```python
dfData_linearPopulation['type'] = 'linearPopulation'
dfData_linearGDP['type'] = 'linearGDP'
dfData_linearEmissions['type'] = 'linearEmissions'
```

```python
dfLinear = pd.DataFrame()
dfLinear = dfLinear.append([dfData_linearPopulation, dfData_linearGDP, dfData_linearEmissions])
dfLinear['downscaleType'] = 'Linear'
```

```python
dfLinear['technology'].unique()
```

```python
dfLinear
```

```python
dfLinear['Year'] = pd.to_datetime(dfLinear['Year'],format='%Y')
```

```python
dfLinear = dfLinear.append(dfCurrent)
```

```python
alt.data_transformers.disable_max_rows()

chart = alt.Chart().mark_line().encode(
    alt.X('Year:T'),
    alt.Y('value:Q', axis=alt.Axis(title='Energy (EJ)')), # format='%', 
    color=alt.Color('type:N', title=['Downscaling','Relative Parameter'])
).properties(
    width=200,
    height=70
).transform_filter(
    (alt.datum.type != 'baseYear')
)

rule = alt.Chart().mark_rule(strokeDash=[3,3]).encode(
    y='value:Q',
    size=alt.SizeValue(1),
    color='type:N'
).transform_filter(
    (alt.datum.type == 'baseYear')
)

# Taken from example https://github.com/altair-viz/altair/issues/920
chart = alt.layer(chart, rule, data=dfLinear).facet(
    facet=alt.Facet('technology:N', title='Resource Type'),
    columns=3
).transform_filter(
    (alt.datum['region'] == countryOfInterest)
).resolve_scale(y='independent')

chart
# Data types are declared with the colon syntax. See more at https://altair-viz.github.io/user_guide/encoding.html#encoding-data-types
```

```python
chart.save('../reports/figures/linearDownscaled_Recategorized.pdf')
```

Normalize scales (easy in Altair)
Maybe categorize by Resource Type

```python
pd.read_csv('../data/processed/linearProportionsEmissions_Recategorized.csv', index_col=0, parse_dates=['Year'])
```

```python
dfEmissionsProportion = pd.read_csv('../data/processed/linearProportionsEmissions_Recategorized.csv', index_col=0, parse_dates=['Year'])
dfGDPProportion = pd.read_csv('../data/processed/linearProportionsGDP_Recategorized.csv', index_col=0, parse_dates=['Year'])
dfPopulationProportion = pd.read_csv('../data/processed/linearProportionsPopulation_Recategorized.csv', index_col=0, parse_dates=['Year'])
```

```python
# Join all the separate proportion DataFrames together
dfLinearProportions = dfEmissionsProportion.query("Year==2018").set_index('geo')['value'].to_frame().rename(columns={'value':'Emissions'})
dfLinearProportions = dfLinearProportions.join(dfPopulationProportion.query("Year==2018").set_index('geo').rename(columns={'value':'Population'})['Population'])
dfLinearProportions = dfLinearProportions.join(dfGDPProportion.query("Year==2018").set_index('geo').rename(columns={'value':'GDP'})['GDP'])
dfLinearProportions = dfLinearProportions.reset_index().rename(columns={'geo':'Country'}).set_index('Country')

# Change the reported value to percents
dfLinearProportions = dfLinearProportions.apply(lambda x: x*100)

dfLinearProportions.to_latex('../data/processed/linearProportions.tex', float_format="{:0.1f}".format)
```

```python
dfLinearProportions = dfLinearProportions.reset_index().melt(id_vars='Country')
```

```python
chart = alt.Chart().mark_bar().encode(
    x=alt.X('Country:N', title='Country'),
    y=alt.Y('value:Q', title="Proportion (%)"),
    color=alt.Color('Country:N', legend=None)
).properties(
    height=100,
    width=400
)

text = chart.mark_text(
    align='center',
    baseline='middle',
    dy=-10  # Vertical offset
).encode(
    text=alt.Text('value:Q', format=',.0f')
)

# Taken from example https://github.com/altair-viz/altair/issues/920
chart = alt.layer(chart, text, data=dfLinearProportions).facet(
    alt.Facet('variable:N', title='Proportional to'), columns=1
)

chart
```

```python
chart.save('../reports/figures/linearProportions.pdf')
```

Between each variable that emissions are downscaled against, each country performs differently. For some countries, the relative difference is small but the absolute difference is large.

```python
chart = alt.Chart(dfLinearProportions).mark_bar().encode(
    x=alt.X('variable:N', title=None), # Need overall x label 'Downscaled Proportional to'
    y=alt.Y('value:Q', title=None), # Need overall y label ... 'Proportion (%)'
    color=alt.Color('Country:N', legend=None),
    column=alt.Column('Country:N', title='Country'),
).properties(
    height=100,
    width=40
).resolve_scale(y='independent') # Make this not independent and normalize!
chart
```

```python
chart.save('../reports/figures/linearProportionsByCountry.pdf')
```

The absolute differences, in terms of standard deviation, are shown below.

```python
chart = alt.Chart(dfLinearProportions.groupby('Country').std().reset_index()).mark_bar().encode(
    x='Country:N',
    y=alt.Y('value:Q', title='Standard Deviation'),
    color=alt.Color('Country:N', legend=None)
).properties(
    height=200,
    width=400
)
chart
```

```python
chart.save('../reports/figures/linearProportionsCountrySTD.pdf')
```

These values are absolute, but we can also examine the relative difference within each group.

```python
chart = alt.Chart(
    (dfLinearProportions.groupby('Country').std() / dfLinearProportions.groupby('Country').mean()).reset_index()
).mark_bar().encode(
    x='Country:N',
    y=alt.Y('value:Q', title='Coefficient of Variation'),
    color=alt.Color('Country:N', legend=None)
).properties(
    height=200,
    width=400
)
chart
```

```python
chart.save('../reports/figures/linearProportionsCountryCoV.pdf')
```

## Convergence Downscaling

```python
dfData_convergenceEnergyGDP = pd.read_csv('../data/processed/convergenceEnergyGDP.csv', index_col=0, parse_dates=['Year'])
dfData_convergenceEnergyPopulation = pd.read_csv('../data/processed/convergenceEnergyPopulation.csv', index_col=0, parse_dates=['Year'])
```

```python
dfData_convergenceEnergyGDP.head()
```

```python
dfData_convergenceEnergyPopulation.head()
```

```python
dfData_convergenceEnergyGDP['type'] = 'convergenceEnergyGDP'
dfData_convergenceEnergyGDP['downscaleType'] = 'convergence'
dfData_convergenceEnergyPopulation['type'] = 'convergenceEnergyPopulation'
dfData_convergenceEnergyPopulation['downscaleType'] = 'convergence'
```

```python
dfConvergence = pd.DataFrame()
dfConvergence = dfConvergence.append([dfData_convergenceEnergyGDP, dfData_convergenceEnergyPopulation])
```

```python
dfConvergence['technology'].unique()
```

```python
dfConvergence.query("technology=='Biomass' & region=='NL'")
```

```python
dfConvergence.query("technology=='Coal'")
```

```python
dfConvergence = dfConvergence.append(dfCurrent).reset_index(drop=True)
```

```python
# Currently done in the previous document
# selection = alt.selection_multi(fields=['geo'], bind='legend')

# chart = alt.Chart(dfConvergence.dropna()).mark_line().encode(
#     alt.X('Year:T'),
#     alt.Y('value:Q', axis=alt.Axis(title='Energy (EJ)')), #format='%', 
#     color='region:N', # This should be type
#     opacity = alt.condition(selection, alt.value(0.6), alt.value(0.2))
# ).properties(
#     width=120,
#     height=70
# ).facet(
#     facet=alt.Facet('technology:N', title='Resource Type'),
#     columns=3
# ).resolve_scale(y='independent').add_selection(selection)

# chart
# # Data types are declared with the colon syntax. See more at https://altair-viz.github.io/user_guide/encoding.html#encoding-data-types
```

```python
# chart.save('../reports/figures/convergence_EU-15.pdf')
```

```python
chart = alt.Chart().mark_line().encode(
    alt.X('Year:T'),
    alt.Y('value:Q', axis=alt.Axis(title='Energy (EJ)')), # format='%', 
    color=alt.Color('type:N', title=['Downscaling','Relative Parameter'])
).properties(
    width=200,
    height=70
).transform_filter(
    (alt.datum.type != 'baseYear') & (alt.datum['region'] == countryOfInterest)
)

rule = alt.Chart().mark_rule(strokeDash=[3,3]).encode(
    y='value:Q',
    size=alt.SizeValue(1),
    color='type:N'
).transform_filter(
    (alt.datum.type == 'baseYear')
)


# Taken from example https://github.com/altair-viz/altair/issues/920
# Better to filter by country here to stay below 5000 rows
chart = alt.layer(chart, rule, data=dfConvergence.query("region==@countryOfInterest")).facet(
    facet=alt.Facet('technology:N', title='Resource Type'),
    columns=3
).resolve_scale(y='independent')

chart
# Data types are declared with the colon syntax. See more at https://altair-viz.github.io/user_guide/encoding.html#encoding-data-types
```

```python
chart.save('../reports/figures/baseRuleVsConvergence_{}.pdf'.format(countryOfInterest))
```

The two convergence methods look almost identical visually. We can look at this in a more sophisticated way.

```python
dfConvergence['diff'] = dfConvergence.reset_index(drop=True).query("downscaleType=='convergence'").groupby(['region','Year','technology'])['value'].diff()
```

```python
dfConvergence
```

```python
dfConvergence[~dfConvergence['diff'].isna()]
```

```python
dfConvergence.groupby(['Year','technology'])['value'].sum()
```

```python
dfConvergence
```

Show the relative difference between these values too – relative to the sum of both methods in that year for each technology.

```python
dfConvergence = dfConvergence.set_index(['Year','technology','region'])
dfConvergence['diffPercent'] = dfConvergence.groupby(['Year','technology','region'])['value'].mean()
dfConvergence = dfConvergence.reset_index().set_index(['Year','technology'])
dfConvergence['groupSum'] = dfConvergence.groupby(['Year','technology'])['value'].sum()

dfConvergence['diffPercent'] = dfConvergence['diff'] / dfConvergence['groupSum'] # Find difference relative to group sum
dfConvergence = dfConvergence.reset_index()
```

```python
dfConvergence[~dfConvergence['diffPercent'].isna()]
```

```python
chart = alt.Chart(dfConvergence[~dfConvergence['diff'].isna()]).mark_line().encode(
    alt.X('Year:T'),
    alt.Y('diff:Q', axis=alt.Axis(title=['Difference in','Energy (EJ)'])),
    color=alt.Color('region:N', title='Country')
).properties(
    width=200,
    height=100
).facet(
    facet=alt.Facet('technology:N', title='Resource Type'),
    columns=3
).resolve_scale(y='independent')

chart
# Data types are declared with the colon syntax. See more at https://altair-viz.github.io/user_guide/encoding.html#encoding-data-types
```

```python
chart.save('../reports/figures/convergenceDiff_Absolute.pdf')
```

```python
chart = alt.Chart(dfConvergence[~dfConvergence['diffPercent'].isna()]).mark_line().encode(
    alt.X('Year:T'),
    alt.Y('diffPercent:Q', axis=alt.Axis(title=['Difference in','Energy'], format='%')),
    color=alt.Color('region:N', title='Country')
).properties(
    width=200,
    height=100
).facet(
    facet=alt.Facet('technology:N', title='Resource Type'),
    columns=3
).resolve_scale(y='independent')

chart
# Data types are declared with the colon syntax. See more at https://altair-viz.github.io/user_guide/encoding.html#encoding-data-types
```

```python
chart.save('../reports/figures/convergenceDiff_Relative.pdf')
```

## External-Input-Downscaling

```python

```

## Comparison of Techniques Under Same Underlying Data


Compare linear to convergence, etc.

```python
pd.DataFrame().append([dfLinear,dfConvergence])
```

```python
dfConvergence.query("region=='NL' & technology=='Hydrogen'")
```

```python
alt.data_transformers.disable_max_rows()

chart = alt.Chart().mark_line(opacity=0.8).encode(
    alt.X('year(Year):T',
          title='Year',
          scale=alt.Scale(
            domain=(baseYear, 2100),
            clamp=True
        )),
    alt.Y('value:Q', axis=alt.Axis(title='Energy (EJ)', format=',.2f')), 
    color=alt.Color('type:N', title='Downscale Type'),
).properties(
    width=120,
    height=70
)

rule = alt.Chart().mark_rule(strokeDash=[3,3]).encode(
    y='value:Q',
    size=alt.SizeValue(1),
    color=alt.Color('type:N')
).transform_filter(
    (alt.datum.type == 'baseYear')
)

chart = alt.layer(chart, rule, data=pd.DataFrame().append([dfLinear,dfConvergence]).query("region==@countryOfInterest")).facet(
    facet=alt.Facet('technology:N', title='Resource Type'),
    columns=3
).resolve_scale(y='independent')

chart
# Renable alt.data_transformers.disable_max_rows?          
# Data types are declared with the colon syntax. See more at https://altair-viz.github.io/user_guide/encoding.html#encoding-data-types
```

None of these visualization approaches understand that technological preferences might have geographic restrictions. It assumes that the Netherlands will rise in using solar thermal and hydroelectricity. Unlike nuclear, these sources have (more) geographical limitations. Nuclear energy also has limitations, since it needs cooling water and is therefore often built next to large bodies of water, but and such bodies of water might have further restrictions. Future nuclear technologies may use alterative methods of cooling, but this is itself an uncertainty.

```python
chart.save('../reports/figures/allCompare_{}.pdf'.format(countryOfInterest))
```

Now do the same but for all the other countries.

```python
alt.data_transformers.disable_max_rows()

for country in dfLinear['region'].unique():
    # Skip the country we have already exported
    if country == countryOfInterest:
        continue

    chart = alt.Chart().mark_line(opacity=0.8).encode(
        alt.X('year(Year):T',
              title='Year',
              scale=alt.Scale(
                domain=(baseYear, 2100),
                clamp=True
            )),
        alt.Y('value:Q', axis=alt.Axis(title='Energy (EJ)', format=',.2')), 
        color=alt.Color('type:N', title='Downscale Type'),
    ).properties(
        width=120,
        height=70
    )

    rule = alt.Chart().mark_rule(strokeDash=[3,3]).encode(
        y='value:Q',
        size=alt.SizeValue(1),
        color=alt.Color('type:N')
    ).transform_filter(
        (alt.datum.type == 'baseYear')
    )

    chart = alt.layer(chart, rule, data=pd.DataFrame().append([dfLinear,dfConvergence]).query("region==@country")).facet(
        facet=alt.Facet('technology:N', title='Resource Type'),
        columns=3
    ).resolve_scale(y='independent')

    chart.save('../reports/figures/allCompare_{}.pdf'.format(country))
# Renable alt.data_transformers.disable_max_rows?
```

## Summary

```python

```
