IAM Statistical Downscaling for Electricity Production
==============================

Data analysis work for [my master thesis](https://repository.tudelft.nl/islandora/object/uuid%3A65aff56a-a83f-4a3c-9575-fe061761b525?collection=education).

The notebooks are organized as such:

|Notebook|Purpose|Data Input|Data Output|
|--|--|--|--|
|0_GCAM Output Exploration|Uses `gcam_reader` to extract data from the GGAM output database into CSVs|Queries from /data/raw|CSV data in /data/interim|
|1_Downscaling Data Retrieval|Retrieves supplementary data from various sources into CSVs|Eurostat, etc.|/data/interim|
|2_Downscale|Downscales data for a user-chosen country|/data/interim|/data/processed|
|3_Visualization|Visually shows downscaling results and discussion|/data/processed|/Figures and tables in reports|
|4_Supporting Data|Auxiliary analyses| ... |Figures and tables in /reports|

Project Organization
------------

    ├── LICENSE
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── notebooks          <- Jupyter notebooks.
    │   └── myFuncs.py     <- File for reused functions.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   ├── figures        <- Generated PDF figures.
    │   └── tables         <- Generated LaTeX tables
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.testrun.org

## Dependencies

### Python

* `pandas` – for data work, of course!
* `eurostat` - API wrapper to access Eurostat databases from Python
* `plotnine` - backup for visualizations
* `altair` - main driver for visualizations
* `altair_saver` – to save visualizations

### Javascript

The below are only required for `altair_saver`. See its [documentation](https://github.com/altair-viz/altair_saver/) for alternatives, including for Windows.

* npm
* vega-lite
* vega-cli
* canvas

<!-- `rpy2` causes problems for some people on macOS because of how it's compiled (it interfaces with R, which uses a newer `clang` than comes with macOS Catalina or older). To resolve this issue, download the source code from a Git repo and run `python setup.py install` like is detailed in the issue [here](https://bitbucket.org/rpy2/rpy2/issues/403/cannot-pip-install-rpy2-with-latest-r-340#comment-45027559). -->

## Issues

* As of June 2020, Altair 4.1.0 is the latest release and has a bug where charts cannot save to PDF. While [the issue is resolved in the GitHub repo](https://github.com/altair-viz/altair/pull/2063#issuecomment-633555268), the fix has not made it to a release yet. While Altair can export to SVGs and then be included into LaTeX through the `svg` package (utilizing `\includesvg{}`), this approach requires Inkscape to convert the SVG file into a PDF. While this is the method implemented, users are advised that any method of turning SVGs into PDFs are sufficient for LaTeX. Alternatively, PNGs should work just fine as well.
* 3_Visualization is a large file and might run very slowly. This issue can be overcome by turning the Notebook into a Python script. In a terminal, navigate to the notebooks directory and use Jupytext  `jupytext --to py *.md`. Then, run `python` on the file. For example, `python x.2\ Visualization.py`.

--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
